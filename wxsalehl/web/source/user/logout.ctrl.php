<?php
/**
 * [WX System] Copyright (c) 2014 176dj.com
 * WX is NOT a free software, it under the license terms, visited http://mp.weixin.qq.com for more details.
 */
defined('IN_IA') or exit('Access Denied');
isetcookie('__session', '', -10000);

$forward = $_GPC['forward'];
if(empty($forward)) {
	$forward = './?refersh';
}
header('Location:' . url('user/login'));
