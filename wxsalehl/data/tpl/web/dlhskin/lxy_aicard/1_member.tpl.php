<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/header', TEMPLATE_INCLUDEPATH)) : (include template('common/header', TEMPLATE_INCLUDEPATH));?>


<ul class="nav nav-tabs">
  <li <?php  if($operation == 'display') { ?>class="active"<?php  } ?>><a href="<?php  echo $this->createWebUrl('member', array('op' => 'display'))?>">管理会员</a></li>
<!--
  <li <?php  if($operation == 'post') { ?>class="active"<?php  } ?>><a href="<?php  echo $this->createWebUrl('member', array('op' => 'post'))?>"><?php  if($id) { ?>编辑<?php  } else { ?>添加<?php  } ?>会员</a></li>
-->
</ul>
<?php  if($operation == 'display') { ?>
<style>
.avatarpic{
	width: 50px;
    height: 50px;
}
</style>
<div class="main panel panel-default">
  <div class="panel-body table-responsive">
    <table class="table table-hover">
      <thead class="navbar-inner">
        <tr>
          <th >会员编号</th>
          <th >MemberId</th>
          <th>会员昵称</th>
          <th>手机号</th>
          <th>性别</th>
          <th>身份证号</th>
          <th style="width:350px;">住址</th>
          <th>头像</th>
<!--
          <th >操作</th>
-->
        </tr>
      </thead>
      <tbody>
      
      <?php  if(is_array($list)) { foreach($list as $row) { ?>
      <tr>
        <td><?php  echo $row['uid'];?></td>
        <td><?php  echo $row['memberId'];?></td>
        <td><?php  echo getUsername($row['realname'])?></td>
        <td><?php  echo $row['phone'];?></td>
        <td><?php  echo chkStatus($row['sex'],'sex')?></td>
        <td><?php  echo $row['idcard'];?></td>
        <td ><?php  echo $row['province'].$row['city'].$row['district'].$row['addr']?></td>

        <td><img src="<?php  echo tomedia($row['avatar'])?>" class="avatarpic" onerror="javascript:this.src='../addons/lxy_decoration/template/images/nopic.png';"></td>
<!--        <td style="text-align:left;"><a href="<?php  echo $this->createWebUrl('member', array('op' => 'post', 'id' => $row['id']))?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="修改"><i class="fa fa-edit"></i></a> <a href="<?php  echo $this->createWebUrl('member', array('op' => 'delete', 'id' => $row['id']))?>"class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="删除"><i class="fa fa-times"></i></a>
        </td>-->
      </tr>
      <?php  } } ?>
        </tbody>
      
    </table>
    <?php  echo $pager;?> </div>
</div>
<script>
	require(['bootstrap'],function($){
		$('.btn').hover(function(){
			$(this).tooltip('show');
		},function(){
			$(this).tooltip('hide');
		});
	});
</script> 
<?php  } else if($operation == 'post') { ?>
<div class="main">
  <form action="" class="form-horizontal form" method="post" enctype="multipart/form-data" onSubmit="return formcheck();">
    <input type="hidden" name="id" value="<?php  echo $item['id'];?>">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4>公司基本信息</h4>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">名称</label>
          <div class="col-sm-9 col-xs-12">
            <input type="text" id="title" name="title" value="<?php  echo $item['title'];?>"  class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">简介</label>
          <div class="col-sm-9 col-xs-12">
            <textarea id="describe"  rows="3" name="describe" class="form-control" ><?php  echo $item['describe'];?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">LOGO</label>
          <div class="col-sm-9 col-xs-12"> <?php  echo tpl_form_field_image('thumb',$item['thumb'])?> </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">地址</label>
          <div class="col-sm-9 col-xs-12">
            <input type="text" id="addr" name="addr" value="<?php  echo $item['addr'];?>"  class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">地区</label>
          <div class="col-sm-9 col-xs-12"> <?php  echo tpl_form_field_district('district',array('province'=>$item['province'],'city'=>$item['city'],'dist'=>$item['dist']))?> </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">地图标识</label>
          <div class="col-sm-9 col-xs-12"> <?php  echo tpl_form_field_coordinate('baidumap',array('lng'=>$item['lat'],'lat'=>$item['lat']))?> </div>
        </div>
      </div>
    </div>
    <div class="form-group col-sm-12">
      <input type="submit" name="submit" value="提交" class="btn btn-primary col-lg-1" />
      <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
    </div>
  </form>
</div>
<script language='javascript'>
	function formcheck() {
		if ($("#title").val()=='') {
			Tip.focus("title", "请填写会员名称!");
			return false;
		}
		return true;
	}
</script> 
<?php  } ?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/footer', TEMPLATE_INCLUDEPATH)) : (include template('common/footer', TEMPLATE_INCLUDEPATH));?>