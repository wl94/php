<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/header', TEMPLATE_INCLUDEPATH)) : (include template('common/header', TEMPLATE_INCLUDEPATH));?>
<style>
     .item_box img{
         width: 100%;
         height: 100%;
     }
</style>
<style type='text/css'>
    .tab-pane { padding:20px 0 20px 0;}
</style>
<div class="main">
	<form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?php  echo $settings['id'];?>" />
    <div class="panel panel-default">
        <div class="panel-heading">
            商家信息
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#tab_basic">基本设置</a></li>
<!--                <li><a href="#tab_settled">入驻设置</a></li>
                <li><a href="#tab_share">分享设置</a></li>-->
            </ul>
            <div class="tab-content">
            <div class="tab-pane  active" id="tab_basic">
<!--                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">网站名称</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[title]" class="form-control" value="<?php  echo $settings['title'];?>" />
                    </div>
                </div>-->
<!--                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">商户页面标题</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[shopsubject]" class="form-control" value="<?php  echo $settings['shopsubject'];?>" />
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">接口地址</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[interfaceurl]" class="form-control" value="<?php  echo $settings['interfaceurl'];?>" />
                    </div>
                </div>
<!--                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">商户页面商家信息称呼</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[shoptitle]" class="form-control" value="<?php  echo $settings['shoptitle'];?>" />
                    </div>
                </div><div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">商户页面商户介绍称呼</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[shopintro]" class="form-control" value="<?php  echo $settings['shopintro'];?>" />
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">列表每页显示数量</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[pagesize]" class="form-control" value="<?php  if(empty($settings)) { ?>5<?php  } else { ?><?php  echo $settings['pagesize'];?><?php  } ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">位置cookie缓存时间</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[cookiemin]" class="form-control" value="<?php  if(empty($settings)) { ?>0<?php  } else { ?><?php  echo $settings['cookiemin'];?><?php  } ?>" />
                        <span class="help-block">获取经纬度获取缓存分钟数</span>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">显示半径</label>
                    <div class="col-sm-9">
                        <input type="text" name="cfgvalue[radius]" class="form-control" value="<?php  if(empty($settings)) { ?>0<?php  } else { ?><?php  echo $settings['radius'];?><?php  } ?>" />
                        <span class="help-block">显示半径为多少米内的商铺,默认为0,全部显示</span>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">小票发送模板</label>
                    <div class="col-sm-9">
                        <select name="cfgvalue[tickettmpid]" class="form-control">
                            <option value="0">请选择</option>
                            <?php  if(is_array($tmps)) { foreach($tmps as $tmp) { ?>
                            <option value="<?php  echo $tmp['id'];?>" <?php  if($settings['tickettmpid']==$tmp['id']) { ?>selected<?php  } ?>><?php  echo $tmp['template_name'];?></option>
                            <?php  } } ?>
                        </select>
                        <span class="help-block">通过微信发送的小票消息模板</span>

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">附近商店微信访问链接</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php  echo $_W['siteroot'].'app/'.substr($this->createMobileUrl('wapstores'),2)?>" disabled />

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">小票接口URL</label>
                    <div class="col-sm-9">
                        <input type="text"  class="form-control" value="<?php  echo $_W['siteroot'].'app/'.substr($this->createMobileUrl('wapsendmsg'),2)?>" disabled />

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">免登陆Url</label>
                    <div class="col-sm-9">
                        <input type="text"  class="form-control" value="<?php echo $_W['siteroot'].'web/indexautoi.php?c=user&a=autologin&username=xxx&pwd=xxx'?>" disabled />
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="form-group col-sm-12">
        <input type="submit" name="submit" value="提交" class="btn btn-primary col-lg-1" />
        <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
    </div>
	</form>
</div>
<script type="text/javascript">
    $(function () {
        window.optionchanged = false;
        $('#myTab a').click(function (e) {
            e.preventDefault();//阻止a链接的跳转行为
            $(this).tab('show');//显示当前选中的链接及关联的content
        })
    });
</script>
<script type="text/javascript">
    require(['jquery', 'util'], function ($, u) {
        $(function () {
            u.editor($('.richtext')[0]);
        });
    });
</script>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/footer', TEMPLATE_INCLUDEPATH)) : (include template('common/footer', TEMPLATE_INCLUDEPATH));?>