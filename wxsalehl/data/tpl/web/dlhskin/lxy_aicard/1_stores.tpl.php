<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/header', TEMPLATE_INCLUDEPATH)) : (include template('common/header', TEMPLATE_INCLUDEPATH));?>
<ul class="nav nav-tabs">
  <li <?php  if($operation == 'post') { ?>class="active"<?php  } ?>><a href="<?php  echo $this->createWebUrl('stores', array('op' => 'post'))?>">添加商家</a></li>
  <li <?php  if($operation == 'display' || empty($operation)) { ?>class="active"<?php  } ?>><a href="<?php  echo $this->createWebUrl('stores', array('op' => 'display'))?>">管理商家</a></li>
</ul>
<?php  if($operation == 'display') { ?>
<link rel="stylesheet" type="text/css" href="<?php echo RES;?>/themes/css/main.css?v=1" />
<div class="main">

  <div class="panel panel-info">
    <div class="panel-heading">筛选</div>
    <div class="panel-body">
      <form action="./index.php" method="get" class="form-horizontal" role="form">
        <input type="hidden" name="c" value="site" />
        <input type="hidden" name="a" value="entry" />
        <input type="hidden" name="m" value="lxy_aicard" />
        <input type="hidden" name="do" value="stores" />
        <input type="hidden" name="op" value="display" />
        <input type="hidden" name="storeid" value="<?php  echo $storeid;?>" />
        <div class="form-group">
          <label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label">关键字</label>
          <div class="col-sm-5 col-lg-5">
            <input class="form-control" name="keyword" id="" type="text" value="<?php  echo $_GPC['keyword'];?>">
          </div>
          <div class="col-sm-3 col-lg-2">
            <button class="btn btn-default"><i class="fa fa-search"></i> 搜索</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <form action="./index.php" method="post" class="form-horizontal form" enctype="multipart/form-data">
    <input type="hidden" name="leadExcel" value="true">
    <input type="hidden" name="c" value="site" />
    <input type="hidden" name="a" value="entry" />
    <input type="hidden" name="m" value="<?php  echo $this->modulename;?>" />
    <input type="hidden" name="do" value="UploadExcel" />
    <input type="hidden" name="ac" value="stores" />
    店铺在线更新:
    <input type="button" class="btn btn-primary span2" name="btnImport" value="更新" onclick="javascript:if(confirm('确认要更新店铺信息？'))window.location.href = '<?php  echo $this->createWebUrl('importstores')?>'; ">
  </form>
  <div style="padding-top: 15px;"></div>


  <ul class="page-nav page-nav-tabs" style="background:none;float: left;margin-left: 0px;padding-left: 0px;border-bottom:1px #c5d0dc solid;">
    <li <?php  if(empty($type)) { ?> class="active"<?php  } ?>> <a href="<?php  echo $this->createWebUrl('stores', array('op' => 'display'))?>">全部商家</a>
    </li>
  </ul>
  <div class="panel panel-default">
    <form action="" method="post" class="form-horizontal form" onsubmit="return formcheck(this)">
      <div class="table-responsive panel-body">
        <table class="table table-hover">
          <thead class="navbar-inner">
          <tr>
            <th style="width:10%;">显示顺序</th>
            <th style="width:10%;">图片</th>
            <th style="width:20%;">名称</th>
            <th style="width:20%;">店长</th>
            <th style="width:20%;">省市</th>
            <th style="width:25%;">地址</th>
            <th style="width:25%;text-align: right;">编辑/删除</th>
          </tr>
          </thead>
          <tbody>

          <?php  if(is_array($list)) { foreach($list as $item) { ?>
          <tr>
            <td><input type="text" class="form-control" name="displayorder[<?php  echo $item['id'];?>]" value="<?php  echo $item['displayorder'];?>"></td>
            <td style="width: 60px;"><img width="80px;" src="<?php  echo tomedia($item['logo']);?>" onerror="this.src='../addons/lxy_aicard/template/themes/images/nopic.jpeg'" style="border-radius: 2px;border-style: solid;border-width: 1px;border-color: rgb(226, 226, 226);"/></td>
            <td> <?php  echo $item['title'];?><br/>
              <?php  if(!empty($category[$item['pcate']])) { ?><span class="text-error">[<?php  echo $category[$item['pcate']]['name'];?>] </span><?php  } ?><?php  if(!empty($children[$item['pcate']][$item['ccate']]['1'])) { ?><span class="text-info">[<?php  echo $children[$item['pcate']][$item['ccate']]['1'];?>] </span><?php  } ?> </td>
            <td><?php  echo $item['storeManager'];?></td>
            <td><?php  echo $item['province'].$item['city']?></td>
            <td><?php  echo $item['address'];?></td>

            <td style="text-align: right;">
              <a class="btn btn-default btn-sm" href="<?php  echo $this->createWebUrl('stores', array('op' => 'post', 'id' => $item['id']))?>" title="编辑"><i class="fa fa-pencil-square-o"></i></a>
              <a class="btn btn-default btn-sm" onclick="return confirm('确认删除吗？');return false;" href="<?php  echo $this->createWebUrl('stores', array('op' => 'delete', 'id' => $item['id']))?>" title="删除"><i class="fa fa-remove"></i></a></td>
          </tr>
          <?php  } } ?>
          </tbody>

          <tfoot>s
          <tr>
            <td colspan="1"><input name="submit" type="submit" class="btn btn-primary" value="批量更新排序">
            <td colspan="1"><input name="export" onclick="javascript:window.location.href='<?php  echo $this->createWebUrl('gen_template',array('type'=>'all'))?>';" type="button" class="btn btn-success" value="批量导出">
              <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" /></td>
          </tr>
          </tfoot>
        </table>
      </div>
    </form>
  </div>
  <?php  echo $pager;?> </div>
<script type="text/javascript">

  <!--

  var category = <?php  echo json_encode($children)?>;

  //-->

</script>
<script type="text/javascript">

  <!--

  function setProperty(obj,id,type){

    $(obj).html($(obj).html() + "...");

    $.post("<?php  echo $this->createWebUrl('setstoresproperty')?>"

            ,{id:id,type:type, data: obj.getAttribute("data")}

            ,function(d){

              $(obj).html($(obj).html().replace("...",""));

              $(obj).attr("data",d.data)

              if(d.result==1){

                $(obj).toggleClass("label-info");

              }

            },"json"

    );

  }

  //-->

</script>
<?php  } else if($operation == 'post') { ?>
<div class="main">
  <script>

    require(['jquery', 'util'], function($, u){

      $('.account p a').each(function(){

        u.clip(this, $(this).text());

      });

    });

  </script>
  <style>
    .needinput{
      color: red;
    }
  </style>
  <form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php  echo $item['id'];?>" />
    <div class="panel panel-default">
      <div class="panel-heading"> 商家信息 </div>
      <div class="panel-body">
        <ul class="nav nav-tabs" id="myTab">
          <li class="active"><a href="#tab_basic">基本信息</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane  active" id="tab_basic">
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">商家名称<span class="needinput">*</span></label>
              <div class="col-sm-9">
                <input type="text" name="title" class="form-control" value="<?php  echo $item['title'];?>" />
                <div class="help-block" style="color:#f00">输入字数请不要超过50个</div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">封面</label>
              <div class="col-sm-9"> <?php  echo tpl_form_field_image('logo', $logo, '../addons/lxy_aicard/template/themes/images/nopic.jpeg', array('width' => 300, 'height' => 150))?> <span class="help-block" style="color:#f00">建议尺寸(300*150)</span> </div>
            </div>

            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">商家描述</label>
              <div class="col-sm-9">
                <textarea rows="20" name="description" class="form-control richtext"><?php  echo $item['description'];?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">联系电话</label>
              <div class="col-sm-9">
                <input type="text" name="tel" id="tel" value="<?php  echo $item['tel'];?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">省</label>
              <div class="col-sm-3">
                <input type="text" name="province" id="province" value="<?php  echo $item['province'];?>" class="form-control">
              </div>
              <label class="col-xs-12 col-sm-1 col-md-1 control-label">市</label>
              <div class="col-sm-3">
                <input type="text" name="city" id="city" value="<?php  echo $item['city'];?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">地址<span class="needinput">*</span></label>
              <div class="col-sm-9">
                <input type="text" name="address" id="address" value="<?php  echo $item['address'];?>" class="form-control">
                <div class="help-block" style="color:#f00">输入字数请不要超过100个</div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">店长</label>
              <div class="col-sm-9">
                <input type="text" name="storeManager" id="storeManager" value="<?php  echo $item['storeManager'];?>" class="form-control">
              </div>
            </div>


            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">状态</label>
              <div class="col-sm-9">
                <label for="isshow1" class="radio-inline">
                  <input type="radio" name="status" value="1" id="isshow1" <?php  if(empty($item) || $item['status'] == 1) { ?>checked="true"<?php  } ?> />
                  显示</label>
                <label for="isshow3" class="radio-inline">
                  <input type="radio" name="status" value="0" id="isshow3"  <?php  if(!empty($item) && $item['status'] == 0) { ?>checked="true"<?php  } ?> />
                  隐藏</label>
                <span class="help-block"></span> </div>
            </div>
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">排序</label>
              <div class="col-sm-9">
                <input type="text" name="displayorder" class="form-control" value="<?php  if(empty($item) || empty($item['displayorder'])) { ?>0<?php  } else { ?><?php  echo $item['displayorder'];?><?php  } ?>" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">坐标</label>
              <div class="col-sm-9"> <?php  echo tpl_form_field_coordinate('baidumap', $item)?> </div>
            </div>
          </div>
          <?php  if(is_array($fields)) { foreach($fields as $key => $filed) { ?>
          <div class="form-group">
            <label class="col-xs-12 col-sm-3 col-md-2 control-label"><?php  echo $filed;?></label>
            <div class="col-sm-9">
              <input type="text" name="cusfield[<?php  echo $key;?>]" class="form-control" value="<?php  echo $arrCus[$key];?>" />
            </div>
          </div>
          <?php  } } ?>
        </div>
      </div>
    </div>
    <div class="form-group col-sm-12">
      <input type="submit" name="submit" value="提交" class="btn btn-primary col-lg-1" />
      <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
    </div>
  </form>
</div>
</div>
<script language='javascript'>

  require(['jquery', 'util'], function ($, u) {

    $(function () {

      u.editor($('.richtext')[0]);

    });

  });

</script>
<script type="text/javascript">

  $(function () {

    window.optionchanged = false;

    $('#myTab a').click(function (e) {

      e.preventDefault();//阻止a链接的跳转行为

      $(this).tab('show');//显示当前选中的链接及关联的content

    })

  });


</script>
<?php  } else if($operation == 'check') { ?>
<div class="main">
  <div class="panel panel-default">
    <form action="" method="post" class="form-horizontal form" onsubmit="return formcheck(this)">
      <div class="table-responsive panel-body">
        <table class="table table-hover">
          <thead>
          <tr>
            <th style="width:10%;">编号</th>
            <th style="width:15%;">名称</th>
            <th style="width:13%;">联系人</th>
            <th style="width:13%;">联系电话</th>
            <th style="width:14%;">申请时间</th>
            <th style="width:10%;">属性</th>
            <th style="width:10%;">操作</th>
          </tr>
          </thead>
          <tbody>

          <?php  if(is_array($list)) { foreach($list as $item) { ?>
          <tr>
            <td><?php  echo $item['id'];?></td>
            <td><?php  echo $item['title'];?></td>
            <td><?php  echo $item['username'];?></td>
            <td><?php  echo $item['tel'];?></td>
            <td><?php  echo date('Y-m-d h:i:s', $item['dateline']);?></td>
            <td> <?php  if($item['checked']==1) { ?> <span class="label" style="background:#56af45;">已审核</span> <?php  } else { ?> <span class="label" style="background:#a61c00;">未审核</span> <?php  } ?> </td>
            <td><a class="btn btn-default btn-sm" href="<?php  echo $this->createWebUrl('stores', array('op' => 'checkdetail', 'id' => $item['id']))?>" title="查看"><i class="fa fa-eye"></i></a> <a class="btn btn-default btn-sm" onclick="return confirm('确认删除吗？');return false;" href="<?php  echo $this->createWebUrl('stores', array('op' => 'delete', 'id' => $item['id']))?>" title="删除"><i class="fa fa-remove"></i></a></td>
          </tr>
          <?php  } } ?>
          </tbody>

        </table>
      </div>
    </form>
    <?php  echo $pager;?> </div>
</div>
<?php  } else if($operation == 'checkdetail') { ?>
<div class="main">
  <form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php  echo $item['id'];?>" />
    <div class="panel panel-default">
      <div class="panel-heading"> 商家信息 </div>
      <div class="panel-body">
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">审核状态</label>
          <div class="col-sm-9">
            <label for="ischeck1" class="radio-inline">
              <input type="radio" name="checked" value="1" id="ischeck1" <?php  if(empty($item) || $item['checked'] == 1) { ?>checked="true"<?php  } ?> />
              已审核</label>
            <label for="ischeck2" class="radio-inline">
              <input type="radio" name="checked" value="0" id="ischeck2"  <?php  if(!empty($item) && $item['checked'] == 0) { ?>checked="true"<?php  } ?> />
              未审核</label>
            <span class="help-block"></span> </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">状态</label>
          <div class="col-sm-9">
            <label for="isshow1" class="radio-inline">
              <input type="radio" name="status" value="1" id="isshow1" <?php  if(empty($item) || $item['status'] == 1) { ?>checked="true"<?php  } ?> />
              显示</label>
            <label for="isshow3" class="radio-inline">
              <input type="radio" name="status" value="0" id="isshow3"  <?php  if(!empty($item) && $item['status'] == 0) { ?>checked="true"<?php  } ?> />
              不显示</label>
            <span class="help-block"></span> </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">商家名称</label>
          <div class="col-sm-9">
            <input type="text" name="title" class="form-control" value="<?php  echo $item['title'];?>" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">商户封面图片</label>
          <div class="col-sm-9">
            <div class="fileupload fileupload-new" data-provides="fileupload">
              <div class="fileupload-preview thumbnail"><?php  if($item['logo']) { ?><img src="<?php  echo tomedia($item['logo'])?>" style="max-width: 540px"/><?php  } ?></div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">用户名称</label>
          <div class="col-sm-9">
            <input type="text" name="username" value="<?php  echo $item['username'];?>" id="username" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">联系电话</label>
          <div class="col-sm-9">
            <input type="text" name="tel" value="<?php  echo $item['tel'];?>" id="tel" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">地址</label>
          <div class="col-sm-9">
            <input type="text" name="address" id="address" value="<?php  echo $item['address'];?>" class="form-control" >
          </div>
        </div>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label">坐标</label>
          <div class="col-sm-9"> <?php  echo tpl_form_field_coordinate('baidumap', $item)?> </div>
        </div>


        <?php  if(is_array($fields)) { foreach($fields as $key => $filed) { ?>
        <div class="form-group">
          <label class="col-xs-12 col-sm-3 col-md-2 control-label"><?php  echo $filed;?></label>
          <div class="col-sm-9">
            <input type="text" name="cusfield[<?php  echo $key;?>]" class="form-control" value="<?php  echo $arrCus[$key];?>" />
          </div>
        </div>
        <?php  } } ?>


      </div>
    </div>
    <div class="form-group col-sm-12">
      <input type="submit" name="submit" value="提交" class="btn btn-primary col-lg-1" />
      <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
    </div>
  </form>
</div>

<?php  } ?>

<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/footer', TEMPLATE_INCLUDEPATH)) : (include template('common/footer', TEMPLATE_INCLUDEPATH));?>