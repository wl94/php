<?php defined('IN_IA') or exit('Access Denied');?>﻿<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>

    <link rel="stylesheet" href="themes/default/lxy_eshopping/Content/index.css">
    <link rel="stylesheet" href="themes/default/lxy_eshopping/Content/swiper-3.2.5.min.css">
    <link rel="stylesheet" type="text/css" href="themes/default/lxy_eshopping/Content/reset.css">
    <link rel="stylesheet" type="text/css" href="themes/default/lxy_eshopping/Content/mainbase.css">
    <link rel="stylesheet" type="text/css" href="themes/default/lxy_eshopping/Content/base.css">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Expires" CONTENT="0">
    <meta http-equiv="Cache-Control" CONTENT="no-cache">
    <meta http-equiv="Pragma" CONTENT="no-cache">
    <script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/template.js"></script>
    <script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/fastclick.js"></script>
    <script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/wapbase.js"></script>
    <script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/jquery.cookie.js"></script>
    <script src="themes/default/lxy_eshopping/Scripts/touchslide.1.1.source.js"></script>
    <script src="themes/default/lxy_eshopping/Scripts/swiper-3.2.5.jquery.min.js"></script>

</head>
<body class="gray-bg" style="padding-bottom: 50px;background-color: #ffffff;">
<div class="navbar">
		<span class="fr sort" onclick="searchpage();"> <i
                class="icon-srh"></i>搜索
		</span>
    <div class="search">
        <i class="fl icon-srh"></i>
        <div class="srh">
            <input type="text" placeholder="在全部产品中查找" id="keyword"
                   style="color: #000;">
        </div>
    </div>
</div>
    <div class="banner" id="banner">
            <ul>
            <?php  if(is_array($advs)) { foreach($advs as $adv) { ?>
                <li><a href="<?php  if(empty($adv['link'])) { ?>#<?php  } else { ?><?php  echo $adv['link'];?><?php  } ?>"><img src="<?php  echo tomedia($adv['thumb']);?>"></a></li>
            <?php  } } ?>
            </ul>
        <ol class="guid"></ol>
    </div>
    <script type="text/javascript">
        TouchSlide({ 
            slideCell:"#banner",
            autoPlay:true,
            delayTime:600,
            interTime:3000,
            titCell:".guid", //开启自动分页titCell 为导航元素包裹层
            mainCell:"ul", 
            effect:"leftLoop", 
            autoPage:true //自动分页
        });
</script>

<div class="main" style="width:100%;margin-left: auto;margin-right: auto;margin-top: 15px;">
<!-- list -->
    <div class="m_list clearfix" >
            <ul>
                <li><a href="<?php  echo $this->createMobileUrl('list2new')?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_03.png"></a><span>全部</span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('isnew'=>1))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_05.png"></a><span>新品</span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('isdiscount'=>1))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_07.png"></a><span>折扣</span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('istime'=>1))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_09.png"></a><span>秒杀</span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('pcate'=>$cfg['homeicon1']))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_15.png"></a><span><?php  echo $cfg['homename1'];?></span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('pcate'=>$cfg['homeicon2']))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_17.png"></a><span><?php  echo $cfg['homename2'];?></span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('pcate'=>$cfg['homeicon3']))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_19.png"></a><span><?php  echo $cfg['homename3'];?></span></li>
                <li><a href="<?php  echo $this->createMobileUrl('list2new',array('pcate'=>$cfg['homeicon4']))?>"><img src="themes/default/lxy_eshopping/Picture/shouye_img_21.png"></a><span><?php  echo $cfg['homename4'];?></span></li>

            </ul>
    </div>
<!-- list -->

<!-- 掌上秒杀 -->
   <div class="module flash scarebuyView hidden" style="display: block;border-top:8px solid #f1f1f1;">
           <h2 class="title clearfix">推荐商品</h2>
           <!--<p class="title2">更多></p>-->

        <div>
            <div class="swiper-loop swiper-container-horizontal">
                <ul class="swiper-wrapper clearfix">
                <?php  if(is_array($rlist)) { foreach($rlist as $item) { ?>
                    <li class="swiper-slide" ><a href="<?php  echo $this->createMobileUrl('detail', array('id' => $item['id']))?>"><img src="<?php  echo tomedia($item['thumb']);?>" onerror="javascript:this.src='themes/default/lxy_eshopping/Picture//nopic.jpg';"></a><p>￥<?php  echo $item['marketprice'];?></p></li>
                 <?php  } } ?>
                </ul>
            </div>
        </div>
    </div>
    <script>
        swiperLoop = $('.swiper-loop').swiper({
            slidesPerView: 3,
            paginationClickable: true
        });
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            loop: true,
            loopAdditionalSlides: 0,
            paginationClickable: true,
            autoplay: 3000,
            observer: true,
            noSwiping: true
        });
    </script>

<!-- 推荐分类 -->

    <div class="img_list clearfix">
    	<?php  $i=1;?>
    	<?php  if(is_array($recommandcategory)) { foreach($recommandcategory as $c) { ?>
        <?php  if($i>3){break;}?>
        <a href="<?php  echo $this->createMobileUrl('list2new',array('pcate'=>$c['id']))?>" <?php  if($i==2) { ?>style="border-left:1px solid #f0f0f0;border-right:1px solid #f0f0f0;"<?php  } ?>>
            <img src="<?php  echo tomedia($c['thumb']);?>">
        </a>
        <?php  $i++;?>
        <?php  } } ?>
    </div>
    <?php  if(count($recommandcategory)>3) { ?>
    <div class="img_list2 clearfix">
        <?php  $i=1;?>
    	<?php  if(is_array($recommandcategory)) { foreach($recommandcategory as $c) { ?>
        <?php  if($i>3) { ?> 
        <a href="http://wsshop.ganghaoyouhuo.com/wap/tmpl/article_item.html?id=1000661" style="border-top: 1px solid #f0f0f0;"><img src="<?php  echo tomedia($c['thumb']);?>" height="182"></a>
        <?php  } ?>
        <?php  } } ?>
    </div>
    <?php  } ?>
    <?php  if($this->module['config']['homelogo']) { ?>
    <div class="banner2">
         <a href="<?php  if($this->module['config']['homeurl']) { ?><?php  echo $this->module['config']['homeurl']?><?php  } else { ?>javascript:void();<?php  } ?>" ><img src="<?php  echo tomedia($this->module['config']['homelogo'])?>"></a>
    </div>
    <?php  } ?>


    <div class="list4">
        <h class="list4_header">热卖商品</h>
        <ul class="list4_li">
        <?php  $i=0;?>
        <?php  if(is_array($hplist)) { foreach($hplist as $hprow) { ?>
        <?php  if($i%2==0) { ?>
            <li class="clearfix">
        <?php  } ?>    
        <a href="<?php  echo $this->createMobileUrl('detail', array('id' => $item['id']))?>" class="like_img clearfix">
                    <div class="like_img_list"><img src="<?php  echo tomedia($hprow['thumb'])?>"  onerror="javascript:this.src='themes/default/lxy_eshopping/Picture//nopic.jpg';" alt=""></div>
                    <div class="like_img_main"><p><span><?php  echo $hprow['title'];?></span></p></div>
                    <h class="clearfix like_img_price"><p>￥<?php  echo $hprow['marketprice'];?></p><span>详细</span></h>
                </a>

                
            <?php  if($i%2==1) { ?>
            </li>
            <?php  } ?>
            
            <?php  $i++;?>
            <?php  } } ?>
            
        </ul>
    </div>

</div>
<?php  $cssflag=1;?>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('footerbar', TEMPLATE_INCLUDEPATH)) : (include template('footerbar', TEMPLATE_INCLUDEPATH));?>

</body>
<script>
    function searchpage() {
        var $s = $('#keyword').val();
        window.location.href = '<?php  echo $this->createMobileUrl('list2new')?>&keyword=' + $s;
    }
</script>
<script src='themes/default/lxy_eshopping/Scripts/ba.js' ></script>
</html>
