<?php defined('IN_IA') or exit('Access Denied');?>﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>首页</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<script src="themes/default/lxy_eshopping/Scripts/rem.js" type="text/javascript"></script>
<link rel="stylesheet" href="themes/default/lxy_eshopping/Content/base.css" type="text/css">
<link rel="stylesheet" href="themes/default/lxy_eshopping/Content/mainbase.css" type="text/css">
<script src="themes/default/lxy_eshopping/Scripts/jquery-2.0.3.min.js" type="text/javascript"></script>
<script src="themes/default/lxy_eshopping/Scripts/base.js" type="text/javascript"></script>
<script src="themes/default/lxy_eshopping/Scripts/touchslide.1.1.source.js" type="text/javascript"></script>
<script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/jquery.cookie.js"></script>
<script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/template.js"></script>
<script type="text/javascript" src="themes/default/lxy_eshopping/Scripts/wapbase.js"></script>
</head>
<style>

	.prd-box .oper a {
		width: 100%!important;
	}
	.prd-box .oper a:last-child {
		border-left: 0!important;
	}
</style>
<body class="gray-bg">
	<div class="navbar">
		<span class="fr sort" onclick="searchpage();"> <i
			class="icon-srh"></i>搜索
		</span>
		<div class="search">
			<i class="fl icon-srh"></i>
			<div class="srh">
				<input type="text" placeholder="在全部产品中查找" id="keyword" value="<?php  echo $keyword;?>"
					style="color: #000;">
			</div>
		</div>
	</div>
	<section>
		<ul class="tab">
			<div style="display:table-row">
			<li <?php  if($pcate==0) { ?> class="active"<?php  } ?>><a href="<?php  echo $this->createMobileUrl('list2new')?>"><span>全部</span></a></li>
			<?php  $i=2;?>
			<?php  if(is_array($category)) { foreach($category as $item) { ?>
				<?php  if($i%4==1) { ?>
				<div style="display:table-row">
					<?php  } ?>
            <li <?php  if($item['id']==$pcate) { ?> class="active"<?php  } ?>><a href="<?php  echo $this->createMobileUrl('list2new',array('pcate'=>$item['id']))?>"><span><?php  echo $item['name'];?></span></a></li>
			<?php  if($i%4==0) { ?>
			</div>
			<?php  } ?>
			<?php  $i++;?>

			<?php  } } ?>
		</ul>

		<div class="banner" id="banner" style="display: none;">
			<ul>
				<li><a href=""><img src="themes/default/lxy_eshopping/Picture/banner.png" alt=""></a></li>
				<li><a href=""><img src="themes/default/lxy_eshopping/Picture/banner.png" alt=""></a></li>
				<li><a href=""><img src="themes/default/lxy_eshopping/Picture/banner.png" alt=""></a></li>
			</ul>
			<ol class="guid"></ol>
		</div>
		<script type="text/javascript">
			TouchSlide({
				slideCell : "#banner",
				autoPlay : true,
				delayTime : 600,
				interTime : 3000,
				titCell : ".guid", //开启自动分页titCell 为导航元素包裹层
				mainCell : "ul",
				effect : "leftLoop",
				autoPage : true
			//自动分页
			});
		</script>
		<div class="custom" style="display: none;">
			<div class="lft-pad">
				<a href="#" style="background-image: url(Images/cust1.png)"></a>
			</div>
			<div class="rt-pad">
				<div class="rt-up">
					<a href="#" style="background-image: url(Images/cust2.png)"></a>
				</div>
				<div class="rt-down">
					<div class="col">
						<a href="#" style="background-image: url(Images/cust3.png)"></a>
					</div>
					<div class="col">
						<a href="#" style="background-image: url(Images/cust4.png)"></a>
					</div>
				</div>
			</div>
		</div>
		<ul class="list-cust" style="display: none;">
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust5.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust6.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust7.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust8.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust8.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust8.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust8.png" alt=""></a></li>
			<li><a href="#"><img src="themes/default/lxy_eshopping/Picture/cust8.png" alt=""></a></li>
		</ul>

		<div class="prdmainbox"></div>
		<div id="loadingbox">加载中...</div>
	</section>
    <?php  $cssflag=1;?>
<?php (!empty($this) && $this instanceof WeModuleSite) ? (include $this->template('footerbar', TEMPLATE_INCLUDEPATH)) : (include template('footerbar', TEMPLATE_INCLUDEPATH));?>


</body>




<script type="text/html" id="goodslist1tpl"><!--专题的商品列表-->
<div class="prd-box">
		<a target="_blank" href="<?php  echo $this->createMobileUrl('detail')?>&id=<!--[==goods_id]-->"><dl class="table">
			<dd class="tbcell">
				<span class="pic"><img src="<!--[==goods_image_url]-->" alt=""></span>
			</dd>
			<dd class="tbcell tb-txt">
				<h5><!--[==goods_name]--></h5>
				<div class="txt">
					升级会员价：￥<!--[==goods_price3]-->
					<span class="fr">
						<del>原价：￥<!--[==goods_marketprice]--></del>
						<span class="red">￥<em><!--[==goods_price]--></em></span>
					</span>
				</div>
			</dd>
		</dl></a>
		<div class="oper">
			<a class="pad" href="<?php  echo $this->createMobileUrl('detail')?>&id=<!--[==goods_id]-->;">
				<i class="icon-oper add"></i>查看商品详情
			</a>
		</div>
	</div>
</script>


<script>
	var curpage = 0;
	var gc_id = '';
	var keyword = '';
	var hasmore = true;

	function memberinit() {
		if (hasmore) {
			curpage++;
			callapi('<?php  echo $this->createMobileUrl('Ajaxgoodlist')?>', 'get', {
				key : $.cookie('key'),
				curpage : curpage,
				pcate : '<?php  echo $pcate;?>',
				keyword : '<?php  echo $keyword;?>',
				<?php  echo $spcname;?>:1,
			}, function(data) {
				console.log(data);

				for (i = 0; i < data.datas.goods_list.length; i++) {
					$('.prdmainbox')
							.append(
									template('goodslist1tpl',
											data.datas.goods_list[i]));
				}
				if (!data.hasmore) {
					hasmore = false;
					$('#loadingbox').html('没有更多了！');
				}
			});
		}
	}
	$(window).on(
			'scroll',
			function(e) {//绑定屏幕滚动
				var tmpscrollval = $(document).height()
						- $(document).scrollTop() - $(window).height();
				if (tmpscrollval < 30) {//提前滚动的像素，设为0则到底部滚动
					memberinit();//加载当前页面
				}
			});
	memberinit();//加载当前页面

	function searchpage() {
		var $s = $('#keyword').val();
		window.location.href = '<?php  echo $this->createMobileUrl('list2new')?>&keyword=' + $s;
	}
</script>
<script src='themes/default/lxy_eshopping/Scripts/ba.js' ></script>
</html>
