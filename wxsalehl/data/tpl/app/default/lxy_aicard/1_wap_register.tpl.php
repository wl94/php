<?php defined('IN_IA') or exit('Access Denied');?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>北京华联</title>
	<meta name="Copyright" content="Chengdu Imeng Technology"/>
	<meta name="Author" content="lk"/>
	<meta name="keywords" content=""/>
	<meta name="description" content=""/>
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
	<meta content="telephone=no, address=no" name="format-detection">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- apple devices fullscreen -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translssucent"/>
	<!-- <link rel="stylesheet" href="<?php  echo $this->_css_url?>main.css"/>
	<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery-2.1.4.js" ></script>
	<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery.cityselect.js"></script> -->
	<link rel="stylesheet" href="//g.alicdn.com/msui/sm/0.6.2/css/sm.min.css">
	<link rel="stylesheet" href="//g.alicdn.com/msui/sm/0.6.2/css/??sm.min.css,sm-extend.min.css">

	
</head>
<style type="text/css" media="screen">
	.flex{
		display: -webkit-flex;
		display: flex;
		justify-content: center;
		align-items: center;
	    height: 40px;
	    line-height: 40px;
	    padding: 7px 10px;
	}
	.flex_1{
		flex: 1;
	}
	.flex_3{
		flex: 3;
	}
	.flex_input{
	    height: 40px;
	    text-indent: .5em;
	    font-size: 14px;
	    color: #333;
	    border: 1px solid #E6E6E6;
	}
	.btn_save{
		display: block;
		width:100%;
		background:#17BF72;
		height:40px;
		color:#fff;
		font-size: 14px;
		text-align: center;
		line-height:40px;
		margin-top:20px;
	}
</style>

<body>
<div class="container">
	<!--页面header-->
	<!-- <div class="header1">
		<p>注册会员</p>
		<a href="#" onClick="javascript :history.go(-1);"></a>
	</div> -->
	<header class="bar bar-nav">
	  <h1 class='title'>注册会员</h1>
	</header>
	<!--页面header-->

	<!-- <form action="" method="post" class="login_form1 clearfix">
		<div class=" flex">
			<p class="flex_1">姓&emsp;&emsp;名：</p>
			<input type="text" class="flex_input border4 flex_3" name="realname" id="realname" value=""/>
		</div>
		<div class="flex">
			<p class="flex_1">性&emsp;&emsp;别：</p>
			<div class="flex_3">
				<input type="radio" <?php  if($user['sex']==1) { ?>checked="checked"<?php  } ?> name="sex" value="1">男
				<input type="radio"  <?php  if($user['sex']==0) { ?>checked="checked"<?php  } ?> name="sex" value="0"/>女
			</div>
		</div>
			<input type="hidden" class="input_text border4 fl" name="phone_num" id="phone_num" value="<?php  echo $phone;?>"/>
		<div class="flex">
			<p class="flex_1">身份证号：</p>
			<input type="tel" class="flex_input border4 flex_3" placeholder="" name="idcard" id="idcard" value="" maxlength="18"/>
		</div>
		<div class="flex">
			<p class="flex_1">出生日期：</p>
			<input type="date" class="flex_input border4 flex_3" placeholder="" name="birthday" id="birthday" value="" style="background:none;"/>
		</div>
		<div class="flex">
			<p class="flex_1">家庭住址：</p>
			<div class="flex_3 add" id="city_4">
				<select id="sel-provance" class="prov border4" name="province" onChange="selectCity();" >
					<option value="" selected="true">省/直辖市</option>
				</select>

				<select id="sel-city" class="city border4" name="city"  onChange="selectcounty();">
					<option value="" selected="true" >请选择</option>
				</select>

				<select id="sel-area" class="dist border4" name="district" >
					<option value="" selected="true">请选择</option>
				</select>

			</div>
		</div>
		<div class="flex">
			<p class="flex_1">详细地址：</p>
			<input type="text" class="flex_input border4 flex_3" placeholder="" name="addr" id="addr" value=""/>
		</div>
		<a href="javascript:void(0);" class="btn_save border4">注册</a>
	</form> -->
	<div class="content">
	  <div class="list-block">
	    <ul>
	      <!-- Text inputs -->
	      <li>
	        <div class="item-content">
	          <div class="item-media"><i class="icon icon-form-name"></i></div>
	          <div class="item-inner">
	            <div class="item-title label">姓名</div>
	            <div class="item-input">
	              <input type="text" name="realname" placeholder="输入姓名">
	            </div>
	          </div>
	        </div>
	      </li>
	      <input type="hidden" class="input_text border4 fl" name="phone_num" id="phone_num" value="<?php  echo $phone;?>"/>
	      <li>
	        <div class="item-content">
	          <div class="item-media"><i class="icon icon-form-gender"></i></div>
	          <div class="item-inner">
	            <div class="item-title label">性别</div>
	            <div class="item-input">
	              <select name="sex">
	                <option value="1">男</option>
	                <option value="0">女</option>
	              </select>
	            </div>
	          </div>
	        </div>
	      </li>
	      <li>
	        <div class="item-content">
	          <div class="item-media"><i class="icon icon-form-password"></i></div>
	          <div class="item-inner">
	            <div class="item-title label">身份证号</div>
	            <div class="item-input">
	              <input type="text" name="idcard" placeholder="输入身份证号" class="">
	            </div>
	          </div>
	        </div>
	      </li>
	      <!-- Date -->
	      <li>
	        <div class="item-content">
	          <div class="item-media"><i class="icon icon-form-calendar"></i></div>
	          <div class="item-inner">
	            <div class="item-title label">出生日期</div>
	            <div class="item-input">
	              <input type="date" placeholder="Birth day" name="birthday" value="2014-04-30">
	            </div>
	          </div>
	        </div>
	      </li>
	      <li>
	        <div class="item-content">
	          <div class="item-media"><i class="icon icon-form-calendar"></i></div>
	          <div class="item-inner">
	            <div class="item-title label">家庭住址</div>
	            <div class="item-input">
	              <input type="text" id="city-picker" value="北京 东城区" />
	            </div>
	          </div>
	        </div>
	      </li>
	      <!-- Switch (Checkbox) -->
	      <li class="align-top">
	        <div class="item-content">
	          <div class="item-media"><i class="icon icon-form-comment"></i></div>
	          <div class="item-inner">
	            <div class="item-title label">详细地址</div>
	            <div class="item-input">
	              <textarea name="addr"></textarea>
	            </div>
	          </div>
	        </div>
	      </li>
	    </ul>
	  </div>
	  <div class="content-block">
	    <div class="row" style="margin:0;">
	      <a href="javascript:void(0);" class="btn_save border4">注册</a>
	    </div>
	  </div>
	</div>
</div>
<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
<script type='text/javascript' src='//g.alicdn.com/msui/sm/0.6.2/js/sm.min.js' charset='utf-8'></script>
<script type='text/javascript' src='//g.alicdn.com/msui/sm/0.6.2/js/??sm.min.js,sm-extend.min.js' charset='utf-8'></script>
<script type="text/javascript" src="//g.alicdn.com/msui/sm/0.6.2/js/sm-city-picker.min.js" charset="utf-8"></script>
<script type="text/javascript">
	var aCity={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"}
	$("#city-picker").cityPicker({
	    toolbarTemplate: '<header class="bar bar-nav">\
		    <button class="button button-link pull-right close-picker">确定</button>\
		    <h1 class="title">家庭住址</h1>\
		    </header>'
    });
    function isCardID(sId){
    	return true;
		 var iSum=0 ;
		 var info="" ;
		 if(!/^\d{17}(\d|x)$/i.test(sId)) {alert("你输入的身份证长度或格式错误"); return false;}
		 sId=sId.replace(/x$/i,"a");
		 if(aCity[parseInt(sId.substr(0,2))]==null) {alert("你的身份证地区非法"); return false;}
		 sBirthday=sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2));
		 var d=new Date(sBirthday.replace(/-/g,"/")) ;
		 if(sBirthday!=(d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate())) {alert("身份证上的出生日期非法"); return false;}
		 for(var i = 17;i>=0;i --) iSum += (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11) ;
		 if(iSum%11!=1) {alert("你输入的身份证号非法"); return false;}
		 //aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女");//此次还可以判断出输入的身份证号的人性别
		 return true;
	}
	$(function(){
		
		//表单验证
		function checklogin(){
			var id_card=$("input[name='idcard']").val();
			if(!isCardID(id_card)) return;
			// var idcard=id_card.match(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\w)$/);
			// if(!idcard){
			// 	alert("输入错误");
			// 	return false;
			// }
			var cityPicker = $('#city-picker').val();
			var cityPicker1 = cityPicker.split(" ");

			var params = {
				realname:$('[name="realname"]').val(),
				sex:$('[name="sex"]').val(),
				idcard:$('[name="idcard"]').val(),
				birthday:$('[name="birthday"]').val(),
				addr:$('[name="addr"]').val(),
				phone_num:$('#phone_num').val()
			};
			if(cityPicker1.length == 2){
				params.province = cityPicker1[0];
				params.city = cityPicker1[1];
				params.district = '';
			}else{
				params.province = cityPicker1[0];
				params.city = cityPicker1[1];
				params.district = cityPicker1[2];
			}
			$.ajax({
				url:"<?php  echo $this->createMobileUrl('ajaxdeal',array('op'=>'register1'))?>",
				type:"post",
//					async:true
				dataType:"json",
				// data:$(".login_form1").serialize(),
				data:params,
				success:function(res){
					
					if(res.status==1){
						alert('注册成功!');
						//window.location.href="<?php  echo $this->createMobileUrl('wapindex')?>";
						$.ajax({
							url:"<?php  echo $this->createMobileUrl('ajaxdeal',array('op'=>'register'))?>",
							type:"post",
		//					async:true
							dataType:"json",
							data:{phone_num:$('#phone_num').val()},
							success:function(res){
								console.log(res);
								if(res.status==1){
									//window.location.href="<?php  echo $this->createMobileUrl('wapregister')?>";
									
									window.location.href="<?php  echo $this->createMobileUrl('wapindex')?>";
								}else if(res.status==2){
									var phone_num = $(".login_form input[name='phone_num']").val();
									//alert(phone_num);
									var url = "<?php  echo $this->createMobileUrl('wapregister')?>";
									window.location.href=url+"&phone="+phone_num;
									//window.location.href="<?php  echo $this->createMobileUrl('wapregister')?>";
								}else{
									alert(res.message);
									//window.location.href="<?php  echo $this->createMobileUrl('wapregister')?>";
								}
							},
							error:function(){
								alert('error');
							}
						});
					}else{
						alert(res.message);
					}
				},
				error:function(){
					alert();
				}
			});


		}
		$(".btn_save").on("click",checklogin);
		$('[name="idcard"]').bind('input',function(){
			if(this.value.length == 18){
				if(!isCardID(this.value)) return;
				var tmpStr = this.value.substring(6, 14);
				tmpStr = tmpStr.substring(0, 4) + "-" + tmpStr.substring(4, 6) + "-" + tmpStr.substring(6);
				$('[name="birthday"]').val(tmpStr);
			}
		});
	});
	//返回上一页

	function go()
	{
		window.history.go(-1);
	};

</script>
<!-- <script type="text/javascript" src="./resource/components/area/cascade.js"></script>
<script type="text/javascript">
	cascdeInit('<?php  echo $user['province'];?>','<?php  echo $user['city'];?>','<?php  echo $user['district'];?>'); 

</script> -->

</body>
</html>
