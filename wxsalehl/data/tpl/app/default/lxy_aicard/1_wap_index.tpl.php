<?php defined('IN_IA') or exit('Access Denied');?><!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<title>北京华联</title>
	<meta name="Copyright" content="Chengdu Imeng Technology"/>
	<meta name="Author" content="lk"/>
	<meta name="keywords" content=""/>
	<meta name="description" content=""/>
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
	<meta content="telephone=no, address=no" name="format-detection">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- apple devices fullscreen -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translssucent"/>
	<link rel="stylesheet" href="<?php  echo $this->_css_url?>main.css"/>
	<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery-2.2.3.js" ></script>
		<script type="text/javascript" src="<?php  echo $this->_script_url?>JsBarcode.all.js"></script>

	</head>
	
	
	<body style="background:#f5f5f5;">
		<div class="container">
			<!--页面header-->
			<div class="header">
		        <p>会员卡专区</p>
		    </div>
			<!--页面header-->
			
			<!--会员卡专区头部内容-->
	        <div class="member_top">
	        	<!--头部-->
	        	<div class="member_top_t clearfix" style="display: flex;align-items: center;padding: 0;">
	            	<!--左边-->
	                <div class="fl left">
	                    <!--头像-->
	                    <img src="<?php  echo $user['avatar']?>" onerror="javascript:this.src='../addons/lxy_aicard/template/img/nopic.png';" class="weichatt_headimg circle">
	                </div>
	                <dl class="fl right" style="margin-top:0;">
	                	<dd style="font-size:14px;"><?php  echo $user['phone'];?></dd>
	                    <dd style="font-size:12px;">零钱包：￥<span id="coinvalue" style="font-size:17px;color:#f35757;"><?php  echo $coin;?></span></dd>
	                    
	                </dl>
	               <!--  <div class="" >
	                    
	                  	<img src="<?php  echo $this->_script_url?>tiaoma.png">
	                </div> -->
	                <!-- <a href="#" class="fl">
	                	金卡会员
	                </a> -->
	                <!-- <?php 
	                    if(is_array($levelList)){
		                    foreach($levelList as $x=>$x_value) {
		                	    echo '等级：'.$x_value['clubName'].'-'.$x_value['memberLevelName'] ;
		                	    
		                	    echo '<br>';
		                	}
	                    }
	                ?> -->
	            </div>
			</div>
			<!--条形码-->
			<div class="barcode" style="padding: 0px!important;">
				<div class="barcode_box" id="showImg" onclick="showImg()">
					<p><span style="color:#f80202;">*</span>付款时，请向收银员出示此条形码</p>
<!--
					<div class="barcode_img"><img src="<?php  echo $this->_img_url?>barcode.png"></div>
-->					<div class="barcode_img" style="    margin-top: -10px;">
					<img id="barcode2" style="width: 91%;height: 100px;"/>
					<p style="padding:0;">点击条码可切换二维码</p>
				</div>

				</div>
			</div>
			
			<!--菜单-->
			<div class="membership_card_nav">
				<div class="cardnav_t clearfix">
					<div class="cardnav_t_l fl">
						<dl class="cardnav_grzl">
		                	<dd style="font-size:14px;color:#666;">个人资料</dd>
		                    <dd style="font-size:14px;color:#333"><a href="<?php  echo $this->createMobileUrl('wapuserinfo')?>">编辑</a></dd>
		                </dl>
					</div>	
					<div class="cardnav_t_r fl">
						<a href="<?php  echo $this->createMobileUrl('wapcoupon')?>">
							<dl class="cardnav_yhq">
			                	<dd style="font-size:14px;color:#666;">优惠券</dd>
			                    <dd style="font-size:14px;color:#333"><?php  echo $couponCount;?></dd>
			                </dl>
			            </a>   
					</div>
				</div>
				<div class="cardnav_b clearfix">
					<div class="cardnav_b_l fl">
						<a href="<?php  echo $this->createMobileUrl('wapscore')?>">
							<dl class="cardnav_xfjf">
			                	<dd style="font-size:14px;color:#666;">消费积分</dd>
			                    <dd style="font-size:14px;color:#333">查看</dd>
			                </dl>
			            </a>    
					</div>	
					<div class="cardnav_b_r fl">
						<a href="<?php  echo $this->createMobileUrl('wapstores')?>">
							<dl class="cardnav_fjmd">
			                	<dd style="font-size:14px;color:#666;">附近门店</dd>
			                    <dd style="font-size:14px;color:#333">查看</dd>
			                </dl>
		                </a>
					</div>
				</div>
			</div>
			
				
			<!--footer-->
			<a href="<?php  echo $this->createMobileUrl('waplogout')?>" class="footer_out">退出</a>
		</div>
	</body>
<script>

	var value = '<?php  echo $memberEncryption?>';
	JsBarcode("#barcode2", value, {
		format:"CODE128",
		displayValue:false,
		fontSize:24,
	});


   function showImg(){
   	window.location.href = "<?php  echo $this->createMobileUrl('wapimg')?>&memberEncryption="+value;
   }

/*
	var barcode = new bytescoutbarcode128();
	var value = '<?php  echo $user['memberId']?>';

	barcode.valueSet(value);
	//barcode.setMargins(5, 5, 5, 5);
	barcode.setBarWidth(0.8);

	var width = barcode.getMinWidth();

	barcode.setSize(width, 100);

	var barcodeImage = document.getElementById('barcodeImage');
	barcodeImage.src = barcode.exportToBase64(width, 100, 0);
*/

/*	$(function () {
		var ajaxurl='<?php  echo $this->createMobileUrl('ajaxdeal',array('op'=>'info'))?>';
		$.post(ajaxurl,{},function (res) {
			if(res.status==1){
				$('#coinvalue').html(res.data.coin);
			}
		},'json');
	});*/
</script>
</html>
