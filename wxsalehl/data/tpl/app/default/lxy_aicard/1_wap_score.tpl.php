<?php defined('IN_IA') or exit('Access Denied');?><!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<title>北京华联</title>
	<meta name="Copyright" content="Chengdu Imeng Technology"/>
	<meta name="Author" content="lk"/>
	<meta name="keywords" content=""/>
	<meta name="description" content=""/>
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
	<meta content="telephone=no, address=no" name="format-detection">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- apple devices fullscreen -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translssucent"/>
	<!-- <link rel="stylesheet" href="<?php  echo $this->_css_url?>main.css"/>
	<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery-2.1.4.js" ></script> -->
	<link rel="stylesheet" href="//g.alicdn.com/msui/sm/0.6.2/css/sm.min.css">
	<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
	<script type='text/javascript' src='//g.alicdn.com/msui/sm/0.6.2/js/sm.min.js' charset='utf-8'></script>
	</head>
	<style type="text/css">
		.block{
			display: none;
		}
	</style>
	
	<body>
		<div class="container">
			<!--页面header-->
			<!-- <div class="header1">
		        <p>消费积分</p>
		        <a href="javascript:history.go(-1);"></a>
		    </div> -->
		    <header class="bar bar-nav">
			  <a class="icon icon-left pull-left" href="javascript:history.go(-1);" style="color:#000;"></a>
			  <h1 class="title">消费积分</h1>
			</header>
			<div class="content">
			  <?php  if(is_array($points)) { foreach($points as $point) { ?>
				  <div class="card" style="margin:.5rem 0;">
				    <div class="card-header"><?php  echo $point['clubName'];?>-<?php  echo $point['orgName'];?></div>
				    <div class="card-content">
				      <div class="card-content-inner">
				      	<div class="row block" <?php  if($point['pointItem']==1 || $point['pointItem']==3) { ?>style="display:block"<?php  } ?>>
					      <div class="col-50">本季度积分：<?php  echo $point['currentPointsSum'];?></div>
					      <div class="col-50">上季度积分：<?php  echo $point['previousPointsSum'];?></div>
					    </div>
					    <div class="row block" <?php  if($point['pointItem']==2 || $point['pointItem']==3) { ?>style="display:block"<?php  } ?>>
					      <div class="col-50">本年度积分：<?php  echo $point['curyearPointsSum'];?></div>
					      <div class="col-50">上年度积分：<?php  echo $point['previousyearPointsSum'];?></div>
					    </div>
				      </div>
				    </div>
				    <div class="card-footer block" <?php  if($point['pointItem']==4) { ?>style="display:block"<?php  } ?>>累计积分：<?php  echo $point['pointsremainSum'];?></div>
				  </div>
			  <?php  } } ?>
			</div>
			<!--页面header-->
			
			<!--内容-->

			<!-- <table width="100%">
				<thead>
					<tr class="bb1">
						<th class="br" width="28%">会员俱乐部</th>
						<th class="br" width="19%">积分余额</th>
						<th class="br" width="26%">本期积分</th>
						<th class="br" width="24%" style="border:0">上期积分</th>
					</tr>
				</thead>
				<tbody>
				<?php  if(is_array($points)) { foreach($points as $point) { ?>
					<tr class="bb2" style="color:rgba(255,255,255,0.8)">
						<td class="br"><?php  echo $point['clubName'];?></td>
						<td class="br"><?php  echo $point['pointsremainSum'];?></td>
						<td class="br"><?php  echo $point['currentPointsSum'];?></td>
						<td class="br" style="border:0"><?php  echo $point['previousPointsSum'];?></td>
					</tr>
				<?php  } } ?>
				</tbody>
			</table> -->
			<!--内容-->
		</div>
	</body>
</html>
