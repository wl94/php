<?php defined('IN_IA') or exit('Access Denied');?><!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<title>北京华联</title>
	<meta name="Copyright" content="Chengdu Imeng Technology"/>
	<meta name="Author" content="lk"/>
	<meta name="keywords" content=""/>
	<meta name="description" content=""/>
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
	<meta content="telephone=no, address=no" name="format-detection">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- apple devices fullscreen -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translssucent"/>
	<link rel="stylesheet" href="<?php  echo $this->_css_url?>main.css"/>
	<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery-2.1.4.js" ></script>
		<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery.cookie.js"></script>
		<script type="text/javascript" charset="utf-8" src="<?php  echo $this->_script_url?>tpl.js"></script>
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=359042E5AC9ced07c553ebe2042aad73"></script>
		<script type="text/javascript" src="<?php  echo $this->_script_url?>position.js?v=<?php echo TIMESTAMP;?>"></script>
	</head>
	
	
	<body style="background:#f5f5f5">
		<div class="container">
			<!--页面header-->
			<div class="header1">
		        <p>附近门店</p>
		        <a href="#" onClick="javascript :history.go(-1);"></a>
		    </div>
			<!--页面header-->
			<input type="hidden" id="curlat" name="curlat" value="0"/>
			<input type="hidden" id="curlng" name="curlng" value="0"/>
			<input type="hidden" id="curmsg" name="curmsg" value="0"/>
			<div id="dweing" style="margin:5px 0 0 5px;display:none;">定位中.....</div>

			<!--内容-->
				<!--循环-->
			<div id="J-wsq-shop">

			</div>
			<!--循环-->
			<p class="addload" id="J-getmore" data-page="1"><i class="icon-spin5" id="J-spin">正在加载中....</i></p>

			<!--内容-->
		</div>
	</body>
	<script id='shop_list' type='text/html'>
		<#for(var i=0;i<shop.length;i++){#>

		<div class="store_introduction clearfix">
			<div class="store_introduction_l fl">
				<img src="<#=shop[i].logo#>" onerror="this.src='<?php  echo $this->_img_url?>nopic.jpeg'"/>
			</div>
			<div class="store_introduction_r fl">
				<p class="store_name"><#=shop[i].title#></p>
				<p class="store_phone"><#=shop[i].tel#></p>
				<a class="td" href="http://apis.map.qq.com/uri/v1/marker?marker=coord:<#=shop[i].lat#>,<#=shop[i].lng#>;title:<#=shop[i].title#>;addr:<#=shop[i].address#>&referer=myapp">
					<p class="store_add"><#=shop[i].address#><span style="color: red">(<#=m2k(shop[i].juli)#>)</span></p>
					</a>
			</div>
		</div>
		<#}#>

	</script>

	<script>
		function map_bd2tx(lat,  lon){
			var tx_lat;
			var tx_lon;
			var x_pi=3.14159265358979324;
			var x = lon - 0.0065, y = lat - 0.006;
			var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
			var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
			tx_lon = z * Math.cos(theta);
			tx_lat = z * Math.sin(theta);
			return {'lat':tx_lat,'lng':tx_lon};
		}

		//获取更多
		$('#J-getmore').on('click', function () {
			get_shops();
		});

		var curpage = 0;

		function get_shops(){

			//执行动画
			$('#J-spin').addClass('animate-spin');
			var loadurl = "<?php  echo $this->createMobileurl('ajaxdeal', array('op' => 'getnearshop'))?>";
			var uid = "30";
			//var page = $(this).attr('data-page');
			curpage++;
			var islast = "";
			var cateson = "";
			var transxy;
			var okfun = function (e) {
				console.log(e.data);



				if (e.data.shop.length <=0) {
					$(".icon-spin5").html('没有更多商家!');
				} else {
					for(var i=0; i<e.data.shop.length; i++)
					{
						transxy=map_bd2tx(e.data.shop[i].lat,e.data.shop[i].lng)

						e.data.shop[i].lat=transxy.lat;
						e.data.shop[i].lng=transxy.lng;
					}



					$("#J-wsq-shop").append(tpl("#shop_list",e.data));
					//结束转动
					$('#J-spin').removeClass('animate-spin');
				}
			};

			$.ajax({
				type: 'POST',
				url: loadurl,
				data: {
					'page': curpage,
					'islast': islast,
					'curlat': $('#curlat').val(),
					'curlng': $('#curlng').val(),
				},
				// type of data we are expecting in return:
				dataType: 'json',
				timeout: 3000,
				context: $('body'),
				success: okfun,
				error: function (xhr) {
					alert('加载更多', '网络通讯失败，请重试!');
				}
			});
			return false;
		}

		function m2k(mile){
			if(parseFloat(mile)>1000){
				var km=mile/1000;
				km=km.toFixed(2);
				return km+'公里';
			}else{
				return mile+'米';
			}
		}

		$(window).on(
				'scroll',
				function(e) {//绑定屏幕滚动
					var tmpscrollval = $(document).height()
							- $(document).scrollTop() - $(window).height();
					if (tmpscrollval < 30) {//提前滚动的像素，设为0则到底部滚动
						get_shops();//加载当前页面
					}
				});

	</script>
	<script language=javascript>
		function go()
		{
			window.history.go(-1);
		};
	</script>

</html>
