<?php defined('IN_IA') or exit('Access Denied');?><!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<title>北京华联</title>
	<meta name="Copyright" content="Chengdu Imeng Technology"/>
	<meta name="Author" content="lk"/>
	<meta name="keywords" content=""/>
	<meta name="description" content=""/>
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<meta content="application/xhtml+xml;charset=UTF-8" http-equiv="Content-Type">
	<meta content="telephone=no, address=no" name="format-detection">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- apple devices fullscreen -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translssucent"/>
	<link rel="stylesheet" href="<?php  echo $this->_css_url?>main.css"/>
	<script type="text/javascript" src="<?php  echo $this->_script_url?>jquery-2.1.4.js" ></script>
	</head>
	
	
	<body style="background: #f5f5f5;">
		<div class="container">
			<!--页面header-->
			<div class="header1">
		        <p>优惠券</p>
		        <a href="#" onClick="javascript :history.go(-1);"></a>
		    </div>
			<!--页面header-->
			
			<!--内容-->
			<div style="padding: 0 10px;">
				<div class="coupon_box">
					<?php  if(is_array($arr)) { foreach($arr as $c) { ?>
					<div class="coupon border6">
						<a href="<?php  echo $this->createMobileUrl('wapcoupondetail',array('accountid'=>$c['AccountId'],'promoid'=>$c['PromoId'],'ctype'=>$c['ctype']))?>" class="coupon_t border4 clearfix" <?php  if($c['ctype']<3) { ?>style="background-color:#65baef;"<?php  } ?>>
							<img src="<?php  echo $this->_img_url?>coupon_bg_left.png" style="position:absolute;top:69px;left:0;width:4px;height:8px;"/>
							<img src="<?php  echo $this->_img_url?>coupon_bg_right.png" style="position:absolute;top:69px;right:0;width:4px;height:8px;"/>
							<p><span>￥</span><?php  echo $c['effectiveMoney'];?></p>
							<dl class="fl">
								<dd style="font-size:15px;"><?php  echo $c['AccountName'];?></dd>
								<dd style="font-size:12px;">领取时间：<?php  echo $c['getDate'];?></dd>
							</dl>
						</a>
						<div class="coupon_b">
							<dd>到期日期：<?php  echo $c['expirationDate'];?></dd>
						</div>
					</div>
					<?php  } } ?>
				</div>
			</div>
			<!--内容-->
		</div>
		<script language=javascript>
			function go()
			{
			window.history.go(-1);
			};
		</script>
	</body>
</html>
