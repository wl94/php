<?php
global $_GPC, $_W;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';

$objMsgs=M('msgs');
/*$item=$objMsgs->fetch(24);
echo date('Y-m-d H:i:s',$item['createtime']);
p(unserialize($item['debuginfo']));*/

$uniacid=$_W['uniacid'];
$mobile=$_GPC['mobile'];
$objMember=M('member');
$toUserinfo=$objMember->fetchcustom(' and phone=:phone ',array(':phone'=>$mobile));

if(!$toUserinfo){
    //json(0,'您要推送的用户没有注册微信会员！');
}
$data=array(
    'mobile'=>$mobile,
    'uniacid'=>$uniacid,
    'uid'=>$toUserinfo['uid'],
    'openid'=>$toUserinfo['openid'],
    'StoreName'=>$_GPC['__input']['storeName'],
    'PosNo'=>$_GPC['__input']['posNo'],
    'DealTime'=>$_GPC['__input']['dealTime'],
    'DealMoney'=>$_GPC['__input']['dealMoney'],
    'ReceiptNo'=>$_GPC['__input']['receiptNo'],
    'uuid'=>uuid(),
    'DealDetail'=>serialize($_GPC['__input']['dealDetail']),
    'createtime'=>TIMESTAMP,
    'debuginfo'=>serialize($_GPC)

);

$objMsgs=M('msgs');
$insertid=$objMsgs->insert($data);

$arrReplace=array(
    '#购物门店#'=>$data['StoreName'],
    '#小票编号#'=>$data['ReceiptNo'],
    '#结算款台#'=>$data['PosNo'],
    '#消费时间#'=>$data['DealTime'],
    '#消费金额#'=>$data['DealMoney']
);

include_once IA_ROOT.'/addons/lxy_aicard/inc/common/tmpmsg.class.php';
$cls_tmp=new wxTemplate($uniacid,$arrReplace);
//获取要通知的用户列表
$totmpid=$this->module['config']['tickettmpid'];
echo $_W['siteroot'];
$url=$_W['siteroot'].'app/'.substr($this->createMobileUrl('wapmsgdetail',array('uuid'=>$data['uuid'])),2);
echo $url;
$sendok=$cls_tmp->send($toUserinfo['openid'],$totmpid,$url);
if($sendok){
    json(1,'success');
}else{
    json(0,'error'.$toUserinfo['openid'].'======='.$totmpid);
}