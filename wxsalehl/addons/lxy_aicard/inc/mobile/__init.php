<?php
global $_W,$_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
if(ISDEBUG) $_W['openid']='fromUser';

$template = 'worker';

if(!$_W['openid']){
	exit('您的登陆信息超时!');
}
load()->model('mc');
$uid = mc_openid2uid($_W['openid']);
$user = mc_fetch($uid,array('nickname','avatar','realname','mobile','gender','residecity','resideprovince'));
$uniacid=$_W['uniacid'];
if(empty($user['nickname']) && !ISDEBUG){
	$user = mc_oauth_userinfo();
}

$sql = "SELECT * FROM ".tablename($_table_member)." WHERE uniacid = :uniacid AND openid = :openid";
$params = array(':uniacid'=>$uniacid,':openid'=>$_W['openid']);
$member = pdo_fetch($sql,$params);

if(empty($member)){
	$data = array();
	$data['uniacid'] = $uniacid;
	$data['openid'] = $_W['openid'];
	$data['nickname'] = $user['nickname'];
	$data['avatar'] = tomedia($user['avatar']);
	$data['time'] = time();
	$data['gender'] = $user['gender'];
	$data['city'] = $user['residecity'];
	$data['province'] = $user['resideprovince'];
	$data['status'] = $_W['fans']['follow'];
	$data['uid'] = $uid;
	pdo_insert($_table_member,$data);
}else{
	$data = array();
	$data['nickname'] = $user['nickname'];
	$data['avatar'] = tomedia($user['avatar']);
	$data['time'] = time();
	$data['status'] = $_W['fans']['follow'];
	$data['uid'] = $uid;
	pdo_update($_table_member,$data,array('id'=>$member['id']));
}


$sql = "SELECT * FROM ".tablename($_table_member)." WHERE uniacid = :uniacid AND openid = :openid";
$params = array(':uniacid'=>$uniacid,':openid'=>$_W['openid']);
$member = pdo_fetch($sql,$params);

$user = array_merge($user,$member);

$share = array();