<?php
// 这个操作被定义用来呈现 功能封面
global $_GPC, $_W;
include MODULE_ROOT.'/inc/mobile/__init.php';
$uniacid = $_W['uniacid'];

if(!$_W['isajax']){
    exit('unknow error');
}
$op=$_GPC['op'];

if($op=='register'){
    $objMember=M('member');

    $mobile=$_GPC['phone_num'];
    $code=$_GPC['code_num'];
    $mark=$_GPC['code'];
    if($code!=$_SESSION['mobile_code'] && $mark==1){
        json(3,'短信验证码错误');
    }
    $g = "/^1[34578]\d{9}$/";
    
    if(preg_match($g,$mobile)){
        $finduser=$objMember->fetchcustom(' and phone=:phone',array(':phone'=>$mobile));
        /*if($finduser){
            json(0,'抱歉!该用户已经注册!');

        }*/
        $data=$this->getInterfaceData('member',$mobile,$uid,$uniacid);
        /*echo $data['code'];*/
        if($data['code']==1000){
            json(0,$data['message']);
        }
        $cardinfo=$data['member'];
        if(!$cardinfo){
            // exit();
            //p('asdaadasda');
            json(2,'抱歉!您不是会员!');
            
            //Header("location:wap_register.html");
        }
        
        $mData=array(
            'realname'=>$cardinfo['name'],
            'memberId'=>$cardinfo['memberId'],
            'phone'=>$mobile,
            'sex'=>$cardinfo['sex'],
            'birthday'=>strtotime($cardinfo['birthday']),
            'memberCardNo'=>$cardinfo['memberCardNo']
        );
        $objMember->update($mData,$user['id']);
        json(1,$user['id']);
    }else{
        json(0,'手机号不正确');
    }
}elseif ($op=='info'){
    $mobile=$user['phone'];
    $data=$this->getInterfaceData('member',$mobile);
    //p($data);
    $info=array(
        'coin'=>floatval($data['coinPurse']),

    );
    json(1,'ok',$info);
}elseif ($op=='userinfo'){

    $userinfo=array(
      'realname'=>$_GPC['realname'],
        'sex'=>intval($_GPC['sex']),
        //'idType' => $_GPC['idType'],
        'idcard'=>$_GPC['idcard'],
        'birthday'=>strtotime($_GPC['birthday']),
        'province'=>$_GPC['province'],
        'city'=>$_GPC['city'],
        'district'=>$_GPC['district'],
        'addr'=>$_GPC['addr'],
    );
    $objMember=M('member');
    $flag=$objMember->update($userinfo,$user['id']);
    if($flag){
        json(1,'ok',$info);
    }else{
        json(0,'您未修改任何内容或者提交失败!');
    }

}elseif ($op=='getnearshop'){

        $curlat = $_GPC['curlat'];
        $curlng = $_GPC['curlng'];

        $setting=$this->module['config'];
        $page = intval($_GPC['page']); //页码
        $pindex = max(1, intval($_GPC['page']));
        $psize = empty($setting) ? 5 : intval($setting['pagesize']);
        $limit=' LIMIT ' . ($pindex - 1) * $psize . ',' . $psize;

        //距离开始
        $juli_str='';
        $juli_where='';
        $radius=floatval($setting['radius']);
        if($curlat && $curlng){
            $juli_str=' , round(6378.138*2*asin(sqrt(pow(sin( (lat*pi()/180-:lat*pi()/180)/2),2)+cos(lat*pi()/180)*cos(:lat*pi()/180)* pow(sin( (lng*pi()/180-:lng*pi()/180)/2),2)))*1000) as juli ';
            if($radius>0){
                $juli_where =" and round(6378.138*2*asin(sqrt(pow(sin( (lat*pi()/180-:lat*pi()/180)/2),2)+cos(lat*pi()/180)*cos(:lat*pi()/180)* pow(sin( (lng*pi()/180-:lng*pi()/180)/2),2)))*1000) <={$radius} ";
            }
        }
        //距离结束

        //商家列表
        $stores = pdo_fetchall("SELECT *,(lat-:lat) * (lat-:lat) + (lng-:lng) * (lng-:lng) as dist {$juli_str} FROM " . tablename($_table_stores) . " WHERE uniacid = :uniacid AND status=1 and checked=1 {$juli_where}  ORDER BY top DESC, displayorder DESC, dist,status DESC,id DESC {$limit} " , array(':uniacid' => $uniacid, ':lat' => $curlat, ':lng' => $curlng));

        $level_star = array(
            '1' => '★',
            '2' => '★★',
            '3' => '★★★',
            '4' => '★★★★',
            '5' => '★★★★★'
        );
        foreach ($stores as $key =>&$value){
            $value['logo']=tomedia($value['logo']);
            $value['level'] = $level_star[$value['level']];
        }
        $args = array('page' => $_GPC['page'], 'pagesize' => 6);

        json(1,'', array('shop' => $stores, 'pagesize' => $args['pagesize']));

}elseif ($op=='inearshop'){
    $curlat = $_GPC['curlat'];
    $curlng = $_GPC['curlng'];
    $setting=$this->module['config'];
    $radius=floatval($setting['radius']);
    $radius=$radius?$radius:1000000;
    $shops=$this->getInterfaceData('storesnearby',$curlat,$curlng,$radius);
    json(1,'',$shops);
}elseif ($op=='register1') {
    $param=array(
      'realname'=>$_GPC['realname'],
        'sex'=>intval($_GPC['sex']),
        'phone'=>$_GPC['phone_num'],
        'idType'=>$_GPC['idType'],
        'idcard'=>$_GPC['idcard'],
        'birthday'=>strtotime($_GPC['birthday']),
        'province'=>$_GPC['province'],
        'city'=>$_GPC['city'],
        'district'=>$_GPC['district'],
        'addr'=>$_GPC['addr'],
        'memberType'=>2,

    );
    $userinfo=json_encode($param);
    $data=$this->getInterfaceData('register',$userinfo,$_W['openid'],$_W['secret']);
    
    if($data['code']==1){
        json(1,'success');
    }else{
        json(2,$data['message']);
    }
}elseif ($op=='sendcode') {

    //random() 函数返回随机整数。
    function randomMsgCode($length = 6 , $numeric = 0) {
        PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
        if($numeric) {
            $hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
        } else {
            $hash = '';
            $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
            $max = strlen($chars) - 1;
            for($i = 0; $i < $length; $i++) {
                $hash .= $chars[mt_rand(0, $max)];
            }
        }
        return $hash;
    }

    $randomCode = randomMsgCode(4,1);

    $_SESSION['mobile_code'] = $randomCode;

    $mobile=$_GPC['phone_num'];
    $flag = 0; 
        //要post的数据 
    $argv = array( 
         'sn'=>'SDK-BBX-010-26829', ////替换成您自己的序列号
         'pwd'=>strtoupper(md5('SDK-BBX-010-26829'.'b668((-2608')), //此处密码需要加密 加密方式为 md5(sn+password) 32位大写
         'mobile'=>$mobile,//手机号 多个用英文的逗号隔开 post理论没有长度限制.推荐群发一次小于等于10000个手机号
         //'content'=>iconv( "UTF-8", "UTF-8//IGNORE" ,'您的本次验证码：1101[华联超市]'),//短信内容
         'content'=>iconv( "UTF-8", "UTF-8//IGNORE" ,'您的本次验证码：'.$randomCode.'[BHG Market Place]'),
         'ext'=>'',     
         'stime'=>'',//定时时间 格式为2011-6-29 11:09:21
         'msgfmt'=>'',
         'rrid'=>''
         ); 
    //构造要post的字符串 
    foreach ($argv as $key=>$value) { 
          if ($flag!=0) { 
             $params .= "&"; 
             $flag = 1; 
          } 
         $params.= $key."="; $params.= urlencode($value); 
         $flag = 1; 
    }

    $params=substr($params, 5);
    $length = strlen($params); 
             //创建socket连接 
    $fp = fsockopen("sdk.entinfo.cn",8061,$errno,$errstr,10) or exit($errstr."--->".$errno); 
     //构造post请求的头 
     $header = "POST /webservice.asmx/mdsmssend HTTP/1.1\r\n"; 
     $header .= "Host:sdk.entinfo.cn\r\n"; 
     $header .= "Content-Type: application/x-www-form-urlencoded\r\n"; 
     $header .= "Content-Length: ".$length."\r\n"; 
     $header .= "Connection: Close\r\n\r\n"; 
     //添加post的字符串 
     $header .= $params."\r\n"; 
     //发送post的数据 
     fputs($fp,$header); 
     $inheader = 1; 
      while (!feof($fp)) { 
         $line = fgets($fp,1024); //去除请求包的头只显示页面的返回数据 
         if ($inheader && ($line == "\n" || $line == "\r\n")) { 
                 $inheader = 0; 
          } 
          if ($inheader == 0) { 
                // echo $line; 
          } 
      } 

          //<string xmlns="http://tempuri.org/">-5</string>
    $line=str_replace("<string xmlns=\"http://tempuri.org/\">","",$line);
    $line=str_replace("</string>","",$line);
    $result=explode("-",$line);
    // echo $line."-------------";
    if(count($result)>1){
        //echo '发送失败返回值为:'.$line.'。请查看webservice返回值对照表';
        json(2, $line.'失败');
    }else{
        //echo '发送成功 返回值为:'.$line; 
        json(1, $line.'==========='.$_SESSION['mobile_code']); 
    } 
    //json(2,'wwww');
            
}elseif ($op=="updateMember") {
  $param=array(
      'realname'=>$_GPC['realname'],
        'sex'=>intval($_GPC['sex']),
        'phone'=>$_GPC['phone'],
        'idType' => $_GPC['idType'],
        'idcard'=>$_GPC['idcard'],
        'birthday'=>strtotime($_GPC['birthday']),
        'province'=>$_GPC['province'],
        'city'=>$_GPC['city'],
        'district'=>$_GPC['district'],
        'addr'=>$_GPC['addr'],
        'id'=>$user['memberId'],
    );
    $userinfo=json_encode($param);
    $data=$this->getInterfaceData('updateMember',$userinfo);
    if($data['code']==1){
        json(1,'success');
    }else{
        json(2,$data['message']);
    }
}

