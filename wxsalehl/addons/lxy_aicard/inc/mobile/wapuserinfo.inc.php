<?php
global $_GPC, $_W;
include MODULE_ROOT.'/inc/mobile/__init.php';

if(!$_W['isajax']){
    //checkAuLogin($uid); // 检查是否已经登陆
    $memberRes=$this->getInterfaceData('queryMember',$user['memberId']);
    $idType = $memberRes['idType'];
    $idTypeName = '';
    if($idType=='99'){
        $idTypeName = '身份证';
    }elseif ($idType=='100') {
        $idTypeName = '军人证';
    }elseif ($idType == '104') {
        $idTypeName = '驾驶证';
    }elseif ($idType == '106') {
        $idTypeName = '护照';
    }else{
        $idTypeName = '其他';
    }
    $cardNo = substr($memberRes['idNo'],0,strlen($memberRes['idNo'])-4).'****';
    
    $data=array(
            'realname'=>$memberRes['name'],
            'mobile' => $memberRes['phone'],
            'idcard' => $memberRes['idNo'],
            'sex' => $memberRes['sex'],
            'province' => $memberRes['mainAddress'],
            'city' => $memberRes['mainAddress'],
            'district' => $memberRes['mainAddress'],
            //'birthday' => $memberRes['birthday'],
            'birthday' => strtotime($memberRes['birthday']),
            //'community' => $memberRes['community'],
            'addr' => $memberRes['detailednessAddress'],
            'idType' => $memberRes['idType'],
            'idTypeName' => $idTypeName,
        );
}elseif($_W['isajax'] && $_GPC['op']=='modify') {
    //checkAuLogin($uid,'ajax');

    $data = array(
        'realname'=>$_GPC['realname'],
        'mobile' => $_GPC['mobile'],
        'sex' => $_GPC['sex'],
        'province' => $_GPC['province'],
        'city' => $_GPC['city'],
        'district' => $_GPC['district'],
        'community' => $_GPC['community'],
        'addr' => $_GPC['addr'],
    );

    $memberObj=M('member');

    $isExistMobile =$memberObj->fetchOtherByMobile($uid,$data['mobile']);
    if ($isExistMobile) {
        json(0,'对不起该手机已经被注册');
    }
    if($memberObj->update($data,$user['id'])){
        json(1,'操作成功.');
    }else{
        json(0,'您未做任何修改!');
    }

}
include $this->template('wap_userinfo');
