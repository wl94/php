<?php
class lxy_aicard_code extends model{
    public $table = 'uni_verifycode';
    private $_uniacid;
    //后去所有
    function __construct()
    {
        global $_W;
        $this->_uniacid=$_W['uniacid'];
    }

    function fetchall($condition='',$params=array(),$keyfield=''){
        $params [':uniacid']=$this->_uniacid;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$condition} ";
        $list = pdo_fetchall($sql,$params,$keyfield);
        return $list;
    }


    //获取分页
    function fetchpageall($pindex=1,$condition='',$params=array(),$psize=15){
        $params [':uniacid']=$this->_uniacid;

        $sql = "SELECT * FROM " . tablename($this->table) . " WHERE uniacid=:uniacid {$condition} ORDER BY id DESC LIMIT " . ($pindex - 1) * $psize . ',' . $psize;
        $list = pdo_fetchall($sql,$params);
        return $list;
    }


    //获取单个
    function fetch($id =''){
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE  id = :id AND uniacid = :uniacid ";
        $params = array(':id'=>$id,':uniacid'=>$this->_uniacid);
        $list = pdo_fetch($sql,$params);
        return $list;
    }

    //根据商品指定条件获取单个
    function fetchcustom($condition='',$params=array()){
        $params [':uniacid']=$this->_uniacid;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE  uniacid = :uniacid {$condition}";
        $list = pdo_fetch($sql,$params);
        return $list;
    }
    //获取总行数
    function fetchtotalnum($condition='',$params=array()){
        $params [':uniacid']=$this->_uniacid;
        $sql = "SELECT count(1) FROM ".tablename($this->table)." WHERE  uniacid = :uniacid {$condition}" ;
        $list = pdo_fetchcolumn($sql,$params);
        return $list;
    }

    //更新
    function update($data,$id){
        return pdo_update($this->table,$data,array('id'=>$id,'uniacid'=>$this->_uniacid));
    }
    //插入
    function insert($data){
        pdo_insert($this->table,$data);
        return pdo_insertid();
    }
    //删除
    function delete($id=''){
        return pdo_delete($this->table,array('id'=>$id,'uniacid'=>$this->_uniacid));
    }
    function deletecustom($condition='',$params=array())
    {
        if($condition) {
            $sql = "delete FROM " . tablename($this->table) . " WHERE  uniacid = :uniacid {$condition}";
            $params['uniacid'] = $this->_uniacid;
            return pdo_query($sql, $params);
        }
        else{
            //donothing
        }
    }

}