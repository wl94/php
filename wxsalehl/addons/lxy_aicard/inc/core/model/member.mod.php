<?php 
class lxy_aicard_member extends model{
	public $table = 'lxy_aicard_member';
	private $_uniacid;
	//后去所有
	function __construct()
	{
		global $_W;
		$this->_uniacid=$_W['uniacid'];
	}

    function fetchall($condition='',$params=array(),$keyfield=''){
        $params [':uniacid']=$this->_uniacid;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$condition} ";
        $list = pdo_fetchall($sql,$params,$keyfield);
        return $list;
    }

	//获取单个
    function fetchById($id=''){
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid AND id = :id";
        $params = array(':uniacid'=>$this->_uniacid,':id'=>$id);
        $list = pdo_fetch($sql,$params);
        return $list;
    }

	function fetch($openid =''){
		$sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid AND openid = :openid";
		$params = array(':uniacid'=>$this->_uniacid,':openid'=>$openid);
		$list = pdo_fetch($sql,$params);
		return $list;
	}
	//获取单个
	function fetchByUid($uid =''){
		$sql = "SELECT * FROM ".tablename($this->table)." WHERE  uid = :uid and uniacid = :uniacid";
		$params = array(':uid'=>$uid,':uniacid'=>$this->_uniacid);
		$list = pdo_fetch($sql,$params);
		return $list;
	}

    //根据商品指定条件获取单个
    function fetchcustom($condition='',$params=array()){
        $params [':uniacid']=$this->_uniacid;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE  uniacid = :uniacid {$condition}";
        $list = pdo_fetch($sql,$params);
        return $list;
    }

	//查找除我之外有没有同样手机号码
	function fetchOtherByMobile($uid='',$mobile=''){
		$sql = "SELECT count(1) FROM ".tablename($this->table)." WHERE  uid <> :uid and mobile=:mobile and  uniacid = :uniacid";
		$params = array(':uid'=>$uid,':mobile'=>$mobile,':uniacid'=>$this->_uniacid);
		$list = pdo_fetchcolumn($sql,$params);
		return $list;

	}
    //获取分页
    function fetchpageall($pindex=1,$condition='',$params=array(),$psize=15,$orderby=''){
        $params [':uniacid']=$this->_uniacid;

        if($orderby==''){
            $oostr=' id DESC ';
        }else{
            $oostr=$orderby;
        }
        $sql = "SELECT * FROM " . tablename($this->table) . " WHERE uniacid=:uniacid {$condition} ORDER BY {$oostr} LIMIT " . ($pindex - 1) * $psize . ',' . $psize;
        $list = pdo_fetchall($sql,$params);
        return $list;
    }

    //获取总行数
    function fetchtotalnum($condition='',$params=array()){
        $params [':uniacid']=$this->_uniacid;
        $sql = "SELECT count(1) FROM ".tablename($this->table)." WHERE  uniacid = :uniacid {$condition}" ;
        $list = pdo_fetchcolumn($sql,$params);
        return $list;
    }


	//更新
	function update($data,$id){
		return pdo_update($this->table,$data,array('id'=>$id));
	}
	//更新
	function updatebyuid($data,$uid){
		return pdo_update($this->table,$data,array('uid'=>$uid));
	}
	//插入
	function insert($data){
		pdo_insert($this->table,$data);
		return pdo_insertid();
	}
	//根据uid数组返回arr[uid][infos]
	function fetchByUids($uids){
		$arrKeys = array_filter($uids);
		if (count($arrKeys) < 1) {
			return array();
		}

		$strTemp = implode(',', $arrKeys);
		$condition = " and uid in ({$strTemp})";
		$arr = pdo_fetchall('select * from ' . tablename($this->table) . " where uniacid=:uniacid {$condition} ", array(':uniacid'=>$this->_uniacid), 'uid');
		return $arr;
	}
}