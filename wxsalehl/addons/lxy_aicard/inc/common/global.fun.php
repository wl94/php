<?php
function json($status='1',$message='系统错误',$data = array()){
    $json = array();
    $json['status'] = $status;
    $json['message'] = $message;
    $json['data'] = $data;
    die(json_encode($json));
}
function p($arr, $isdie = true)
{
    echo '<pre>';
    print_r($arr);
    if ($isdie) {
        die();
    }
}
function create_guid() {
    $charid = strtoupper(md5(uniqid(mt_rand(), true)));
    $hyphen = chr(45);// "-"
    $uuid = substr($charid, 0, 8).$hyphen
        .substr($charid, 8, 4).$hyphen
        .substr($charid,12, 4).$hyphen
        .substr($charid,16, 4).$hyphen
        .substr($charid,20,12);
    return $uuid;
}

function checkAuLogin($uid, $type = '', $msg = '')
{
    $msg = $msg ? $msg : '无法获取您的个人微信openid!';

    if ($type != 'ajax') {
        if (empty($uid)) {
            message($msg);
            die();
        }
    } else {
        if (empty($uid)) {
            json('1',$msg);
        }
    }

    return true;
}

function M($name){
    static $model = array();
    if (isset($model[$name])) {
        return $model[$name];
    }
    $fileUrl = IA_ROOT.'/addons/lxy_aicard/inc/core/model/'.$name.'.mod.php';
    if (!is_file($fileUrl)) {
        die(' Model ' . $name . ' Not Found!');
    }
    require $fileUrl;
    $class_name = 'lxy_aicard_' . strtolower($name);
    $model[$name] = new $class_name();
    return $model[$name];
}
/*
 * 显示审核状态
 */
function chkStatus($arrflag, $type = 'status') {
    $chkflag=is_array($arrflag)?$arrflag[0]:$arrflag;
    //附属判断条件
    $extraFlag1=is_array($arrflag)?$arrflag[1]:'';
    $extraFlag2=is_array($arrflag)?$arrflag[2]:'';

    if ($type=='sex'){
        $arrWorkerstatus=array(
            '0'=>'女',
            '1'=>'男',
        );
        return $arrWorkerstatus[$chkflag];
    }elseif ($type=='ctype'){
        $arrWorkerstatus=array(
            '1'=>'积分返现券',
            '2'=>'积分返利金',
            '3'=>'优惠券',
        );
        return $arrWorkerstatus[$chkflag];
    }


}




function getuids($list){
    $arr=array();
    foreach ($list as $item) {
        if(!in_array($item['uid'],$arr)){
            $arr[]=$item['uid'];
        }
    }
    return $arr;
}

function gettaskids($list){
    $arr=array();
    foreach ($list as $item) {
        if(!in_array($item['taskid'],$arr)){
            $arr[]=$item['taskid'];
        }
    }
    return $arr;

}

function getUsername($name){
    return $name?$name:'匿名';
}

function getUsernameAuto($item){
    $name='';
    if($item['realname']){
        $name=$item['realname'];
    }else{
        $name=$item['nickname'];
    }
    return $name?$name:'匿名';
}

function gettimestr($time,$showtype=1){
    if($time){
        if($showtype==1){
            $str=date('Y-m-d H:i:s',$time);
        }elseif($showtype==2){
            $str=date('Y-m-d',$time);
        }elseif($showtype==3){
            $str=date('m-d H:i:s',$time);
        }


    }else{
        $str='-';
    }
    return $str;
}

function uuid() {
    if (function_exists ( 'com_create_guid' )) {
        return com_create_guid ();
    } else {
        mt_srand ( ( double ) microtime () * 10000 ); //optional for php 4.2.0 and up.随便数播种，4.2.0以后不需要了。
        $charid = strtoupper ( md5 ( uniqid ( rand (), true ) ) ); //根据当前时间（微秒计）生成唯一id.
        //$hyphen = chr ( 45 ); // "-"
        $hyphen='';
        $uuid = '' . //chr(123)// "{"
            substr ( $charid, 0, 8 ) . $hyphen . substr ( $charid, 8, 4 ) . $hyphen . substr ( $charid, 12, 4 ) . $hyphen . substr ( $charid, 16, 4 ) . $hyphen . substr ( $charid, 20, 12 );
        //.chr(125);// "}"
        return $uuid;
    }
}
