<?php

/**
 * Created by PhpStorm.
 * User: jasonwolf
 * Date: 22/5/16
 * Time: PM5:43
 */
class wxTemplate
{
    private $_arrReplace;
    private $_uniacid;

    public function wxTemplate($uniacid,$arr=array()){
        $this->_uniacid=$uniacid;
        $this->_arrReplace=$arr;

    }
    public function setReplaceArray($arr){
        $this->_arrReplace=$arr;
    }

    public function send($openid, $tmplmsgid,$appUrl="#",$pkey='',$stepid='',$isCheck=false) {

        $tmplmsg = pdo_fetch ( "select * FROM " . tablename ( "lxy_aicard_tmplmsg" ) . " where id = :id and uniacid=:uniacid ", array (
            ':id' => $tmplmsgid,':uniacid'=>$this->_uniacid
        ) );

        if($isCheck && $pkey!='' && $stepid!=''){
            //查找日志是否发送过
            $isSended=pdo_fetchcolumn('select count(1) from '.tablename('lxy_aicard_fanstmplmsg')." where from_user=:from_user and  tmplmsgid=:tmplmsgid and pkey=:pkey and stepid=:stepid",array(':from_user'=>$openid,':tmplmsgid'=>$tmplmsgid,':pkey'=>$pkey,':stepid'=>$stepid));
            if($isSended){
                return false;
            }

        }

        if (! empty ( $tmplmsg )) {
            $datas ['first'] = array (
                'value' => strtr ( $tmplmsg ['first'], $this->_arrReplace ),
                'color' => $tmplmsg ['firstcolor']
            );
            for($i = 1; $i <= 10; $i ++) {
                if (! empty ( $tmplmsg ['keyword' . $i] ) && ! empty ( $tmplmsg ['keyword' . $i . 'code'] )) {
                    $datas [$tmplmsg ['keyword' . $i . 'code']] = array (
                        'value' => strtr ( $tmplmsg ['keyword' . $i], $this->_arrReplace ),
                        'color' => $tmplmsg ['keyword' . $i . 'color']
                    );
                }
            }
            $datas ['remark'] = array (
                'value' => strtr ( $tmplmsg ['remark'], $this->_arrReplace ),
                'color' => $tmplmsg ['remarkcolor']
            );
            $data = json_encode ( $datas );

            load ()->func ( 'communication' );
            load ()->classs ( 'weixin.account' );
            $accObj = WeixinAccount::create ( $this->_uniacid );
            $access_token = $accObj->fetch_token ();
            if (empty ( $access_token )) {
                return;
            }
            $postarr = '{"touser":"' . $openid . '","template_id":"' . $tmplmsg ['template_id'] . '","url":"' . $appUrl . '","topcolor":"' . $tmplmsg ['topcolor'] . '","data":' . $data . '}';
            $res = ihttp_post ( 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=' . $access_token, $postarr );
            // 添加消息发送记录
            $tmplmsgdata = array (
                'uniacid' => $this->_uniacid,
                'pkey'=>$pkey,
                'stepid'=>$stepid,
                'from_user' => $openid,
                'tmplmsgid' => $tmplmsgid,
                'tmplmsg' => $postarr,
                'sendtype'=>'tmp',
                'createtime' => TIMESTAMP
            );
            pdo_insert ( 'lxy_aicard_fanstmplmsg', $tmplmsgdata );
            // 添加消息发送记录
            return true;
        }
        return;
    }

}