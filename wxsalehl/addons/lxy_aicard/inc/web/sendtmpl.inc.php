<?php
/**
 * Created by PhpStorm.
 * User: jasonwolf
 * Date: 10/11/16
 * Time: PM4:15
 */
global $_W,$_GPC;
$uniacid=$_W['uniacid'];
$mobile=$_GPC['mobile'];
if(!preg_match("/^1[34578]{1}\d{9}$/",$mobile)){
    die(json(0,"手机号格式错误！"));
}


$ticketdata=json_decode($_GPC['ticketinfo'],1);
if(!$ticketdata){
    die(json(0,"您的小票信息为空！"));

}
$uuid=create_guid();
$data=array(
    'uniacid'=>$uniacid,
    'guid'=>$uuid,
    'info'=>serialize($ticketdata),
    'createtime'=>TIMESTAMP
)
$objTicket=M('ticket');
$objTicket->insert($data);


