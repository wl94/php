<?php
global $_W, $_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include_once MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
checklogin();

$uniacid=$_W['uniacid'];
load()->func('tpl');


$modulename = $this->modulename;
    $fields=$this->get_fields();
    $operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
    $setting=$this->module['config'];
$objStores=M('stores');

    if ($operation == 'post') {
        $id = intval($_GPC['id']);
        if (!empty($id)) {
            $item =$objStores->fetch($id);
            $arrCus=unserialize($item['cusfield']);

            if (empty($item)) {
                message('抱歉，商家不存在或是已经删除！', '', 'error');
            }
        }

        if (!empty($item)) {
            $logo = tomedia($item['logo']);
        }

        if (checksubmit('submit')) {
            $data = array(
                'uniacid' => $uniacid,
                'displayorder' => intval($_GPC['displayorder']),
                'title' => trim($_GPC['title']),
                'description' => trim($_GPC['description']),
                'tel' => trim($_GPC['tel']),
                'address' => trim($_GPC['address']),
                'lng' => trim($_GPC['baidumap']['lng']),
                'lat' => trim($_GPC['baidumap']['lat']),
                'status' => intval($_GPC['status']),
                'cusfield'=>serialize($_GPC['cusfield']),
                'dateline' => TIMESTAMP,

            );
            if (empty($data['title'])) {
                message('请输入商家名称！');
            }
            if (empty($data['address'])) {
                message('请输入商家地址！');
            }
            if (!empty($_GPC['logo'])) {
                $data['logo'] = $_GPC['logo'];
            }

            if (empty($id)) {
                $objStores->insert($data);
            } else {
                unset($data['dateline']);
                $objStores->update($data,$id);
            }
            message('数据更新成功！', $this->createWebUrl('stores', array('op' => 'display')), 'success');
        }
    } elseif ($operation == 'display') {
        $type = intval($_GPC['type']);
        if (!empty($_GPC['displayorder'])) {
            foreach ($_GPC['displayorder'] as $id => $displayorder) {
                $objStores->update(array('displayorder' => $displayorder),$id);
            }
            message('排序更新成功！', $this->createWebUrl('stores', array('op' => 'display')), 'success');
        }

        $check_shop_count1 =$objStores->fetchtotalnum(' AND mode=1 ',array());
        $check_shop_count2 =$objStores->fetchtotalnum(' AND mode=1 AND checked=0 ',array());
        $totalcount = $objStores->fetchtotalnum();
        $endcount = $totalcount;

        $pindex = max(1, intval($_GPC['page']));
        $psize = 10;
        $condition = '';
        if (!empty($_GPC['keyword'])) {
            $condition .= " AND title LIKE '%{$_GPC['keyword']}%'";
        }

        if (isset($_GPC['status'])) {
            $condition .= " AND status = '" . intval($_GPC['status']) . "'";
        }
        //推荐商家
        if ($type == 2) {
            $condition .= " AND top = 1 ";
        }


        $list=$objStores->fetchpageall($pindex,$condition,array(),$psize);

        if (!empty($list)) {
            $total = $objStores->fetchtotalnum($condition,array());
            $pager = pagination($total, $pindex, $psize);
        }
    } elseif ($operation == 'delete') {
        $id = intval($_GPC['id']);
        $row = pdo_fetch("SELECT * FROM " . tablename($this->table_stores) . " WHERE id = :id", array(':id' => $id));
        if (empty($row)) {
            message('抱歉，数据不存在或是已经被删除！');
        }

        pdo_delete($this->table_stores, array('id' => $id));
        message('删除成功！', $this->createWebUrl('stores', array('op' => 'display')), 'success');
    } elseif ($operation == 'check') {
        if (!empty($_GPC['displayorder'])) {
            foreach ($_GPC['displayorder'] as $id => $displayorder) {
                pdo_update($this->table_stores, array('displayorder' => $displayorder), array('id' => $id));
            }
            message('排序更新成功！', $this->createWebUrl('stores', array('op' => 'display')), 'success');
        }

        $pindex = max(1, intval($_GPC['page']));
        $psize = 10;
        $condition = '';
        if (!empty($_GPC['keyword'])) {
            $condition .= " AND title LIKE '%{$_GPC['keyword']}%'";
        }

        if (isset($_GPC['status'])) {
            $condition .= " AND status = '" . intval($_GPC['status']) . "'";
        }

        $list = pdo_fetchall("SELECT * FROM " . tablename($this->table_stores) . " WHERE uniacid = '{$_W['uniacid']}' AND mode=1 $condition ORDER BY checked, displayorder DESC, id DESC LIMIT " . ($pindex - 1) * $psize . ',' . $psize);

        if (!empty($list)) {
            $total = pdo_fetchcolumn('SELECT COUNT(1) FROM ' . tablename($this->table_stores) . " WHERE uniacid = '{$_W['uniacid']}' AND mode=1 $condition");
            $pager = pagination($total, $pindex, $psize);
        }
    } else if ($operation == 'checkdetail') {
        $id = intval($_GPC['id']);
        $fields=$this->get_fields();

        if (!empty($id)) {
            $item = pdo_fetch("SELECT * FROM " . tablename($this->table_stores) . " WHERE id = :id", array(':id' => $id));
            $arrCus=unserialize($item['cusfield']);

            if (empty($item)) {
                message('抱歉，商家不存在或是已经删除！', '', 'error');
            }
        }
        if (checksubmit('submit')) {
            $data = array(
                'checked' => intval($_GPC['checked']),
                'status' => intval($_GPC['status']),
                'title' => trim($_GPC['title']),
                'username' => trim($_GPC['username']),
                'tel' => trim($_GPC['tel']),
                'address' => trim($_GPC['address']),
                'lng' => trim($_GPC['baidumap']['lng']),
                'lat' => trim($_GPC['baidumap']['lat']),
                'cusfield'=>serialize($_GPC['cusfield']),

            );
            pdo_update($this->table_stores, $data, array('id' => $id));
            message('数据更新成功！', $this->createWebUrl('stores', array('op' => 'check')), 'success');
        }
    }
    include $this->template('stores');
