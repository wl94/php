<?php
global $_W, $_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include_once MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
checklogin();

$uniacid=$_W['uniacid'];
load()->func('tpl');
$type=$_GPC['type'];

$type=in_array($type,array('template','all'))?$type:'template';

$objStores=M('stores');
$filename=$type=='template'?'sample'.$uniacid.'.xls':'stores'.TIMESTAMP.'.xls';
require_once "../framework/library/phpexcel/PHPExcel.php";
$objPHPExcel = new PHPExcel();

$fields=$this->get_fields();
$cusFieldsCount=count($fields);//自定义字段数量
$arrTitle=array('商家名称','商家描述','联系人','联系电话	','联系地址','经度','纬度');

if($type=='template') {
    $excel_data = array(
        '潮汕牛肉丸',
        '潮汕牛肉丸',
        '陈先生',
        '13623009195',
        '汕头市大学路108',
        '123.1',
        '39.225',
    );
    foreach ($fields as $key=>$value){
        $arrTitle[]=$value;
        $excel_data[] = '';
    }

}else{
    $excel_data=$objStores->fetchall();

    $arrStores=array();
    foreach ($excel_data as $key=>$value){
        $arrStores[$key][]=$value['title'];
        $arrStores[$key][]=$value['description'];
        $arrStores[$key][]=$value['username'];
        $arrStores[$key][]=$value['tel'];
        $arrStores[$key][]=$value['address'];
        $arrStores[$key][]=$value['lng'];
        $arrStores[$key][]=$value['lat'];
        $cusRow=unserialize($value['cusfield']);
        for($i=0;$i<$cusFieldsCount;$i++) {
            $arrStores[$key][] = $cusRow[$i];
        }

    }
}

// 操作第一个工作表
$objPHPExcel->setActiveSheetIndex ( 0 );
// 设置工作薄名称
$objPHPExcel->getActiveSheet ()->setTitle ( iconv ( 'gbk', 'utf-8', 'phpexcel测试' ) );
// 设置默认字体和大小
$objPHPExcel->getDefaultStyle ()->getFont ()->setName ( iconv ( 'gbk', 'utf-8', '宋体' ) );
$objPHPExcel->getDefaultStyle ()->getFont ()->setSize ( 10 );

//标题
foreach ( $arrTitle as $key => $value ) {
    $objPHPExcel->getActiveSheet ()->setCellValue ( $this->get_pos($key,1), $value );
}
//内容
if($type=='template') {
    foreach ($excel_data as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue($this->get_pos($key, 2), $value);
    }
}else{
    foreach ($arrStores as $key => $value) {
        foreach ($value as $k=>$v){
            $objPHPExcel->getActiveSheet()->setCellValue($this->get_pos($k, 2+$key), $v);
        }
    }

}

// Rename worksheet
$time=time();
if($type=='template') {
    $objPHPExcel->getActiveSheet()->setTitle($_W['account']['name'] . '商户导入模板' . $time);
}else{
    $objPHPExcel->getActiveSheet()->setTitle($_W['account']['name'] . '全部商户导出' . $time);

}


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)]

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
