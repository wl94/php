<?php
global $_W,$_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include_once MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
checklogin();

$uniacid=$_W['uniacid'];

$data=$this->getInterfaceData('storesAll','');

if(!$data){
    message('接口获取信息错误!');
}
$stores=$data['stores'];
if(!$stores){
    message('店铺数据为空!');
}
$objStores=M('stores');
$iUpdate=0;
$iInsert=0;
foreach ($stores as $store){
    $info=array(
        'title'=>$store['storeName'],
        'storeRegisteredName'=>$store['storeRegisteredName'],
        'storeOpenDate'=>date('Y-m-d H:i:s',strtotime($store['storeOpenDate'])),
        'uniacid'=>$uniacid,
        'lat'=>$store['longitude'],
        'lng'=>$store['latitude'],
        'status'=>1,
        'address'=>$store['address'],
        'storeManager'=>$store['storeManager'],
        'province'=>$store['province'],
        'city'=>$store['city'],
        'dateline'=>TIMESTAMP,
    );

    $findstore=$objStores->fetchcustom(' and store=:store ',array(':store'=>$store['store']));
    if($findstore){
        if($objStores->update($info,$findstore['id']))$iUpdate++;
    }else{
        $info['store']=$store['store'];
        if($objStores->insert($info))$iInsert++;
    }
}
message("更新成功{$iUpdate}家,新增层高{$iInsert}家店铺!",referer(),'success');



