<?php
defined('IN_IA') or exit('Access Denied');
// 消息模板管理
global $_GPC, $_W;
checklogin();
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';

global $_GPC, $_W;
$op=$_GPC['op']?$_GPC['op']:'display';
$id = intval ( $_GPC ['id'] );
$uniacid=$_W['uniacid'];
if(!in_array($op,array('display','post','delete'))){
    $op='display';
}
$objTmplmsg=M('tmplmsg');
if($op=='display'){
    // 消息模板管理

    if (! empty ( $_GPC ['keyword'] )) {
        $where = " AND template_name LIKE  '%{$_GPC['keyword']}%' ";
    }
    $total=$objTmplmsg->fetchtotalnum($where);
    $pindex = max ( 1, intval ( $_GPC ['page'] ) );
    $psize = 20;
    $pager = pagination ( $total, $pindex, $psize );
    $start = ($pindex - 1) * $psize;
    $list=$objTmplmsg->fetchpageall($pindex,$where,array(),$psize);
    include $this->template ( 'tmplmsg' );
}
elseif($op=='post'){
// 消息模板修改
    load ()->func ( 'tpl' );
    if (! empty ( $id )) {
        $item=$objTmplmsg->fetch($id);
    }
    if (checksubmit ( 'submit' )) {
        if (empty ( $_GPC ['template_name'] )) {
            message ( '消息模板名称必需输入', referer (), 'error' );
        }
        if (empty ( $_GPC ['template_id'] )) {
            message ( '消息模板ID必需输入', referer (), 'error' );
        }
        if (empty ( $_GPC ['first'] )) {
            message ( '消息模板标题必需输入', referer (), 'error' );
        }
        if (empty ( $_GPC ['keyword1'] )) {
            message ( '消息模板必需输入一个参数', referer (), 'error' );
        }
        if (empty ( $_GPC ['remark'] )) {
            message ( '消息模板必需输入备注', referer (), 'error' );
        }
        $data = array (
            'uniacid' => $uniacid,
            'template_name' => $_GPC ['template_name'],
            'template_id' => $_GPC ['template_id'],
            'topcolor' => $_GPC ['topcolor'],
            'first' => $_GPC ['first'],
            'firstcolor' => $_GPC ['firstcolor'],
            'keyword1' => $_GPC ['keyword1'],
            'keyword2' => $_GPC ['keyword2'],
            'keyword3' => $_GPC ['keyword3'],
            'keyword4' => $_GPC ['keyword4'],
            'keyword5' => $_GPC ['keyword5'],
            'keyword6' => $_GPC ['keyword6'],
            'keyword7' => $_GPC ['keyword7'],
            'keyword8' => $_GPC ['keyword8'],
            'keyword9' => $_GPC ['keyword9'],
            'keyword10' => $_GPC ['keyword10'],
            'keyword1color' => $_GPC ['keyword1color'],
            'keyword2color' => $_GPC ['keyword2color'],
            'keyword3color' => $_GPC ['keyword3color'],
            'keyword4color' => $_GPC ['keyword4color'],
            'keyword5color' => $_GPC ['keyword5color'],
            'keyword6color' => $_GPC ['keyword6color'],
            'keyword7color' => $_GPC ['keyword7color'],
            'keyword8color' => $_GPC ['keyword8color'],
            'keyword9color' => $_GPC ['keyword9color'],
            'keyword10color' => $_GPC ['keyword10color'],
            'keyword1code' => $_GPC ['keyword1code'],
            'keyword2code' => $_GPC ['keyword2code'],
            'keyword3code' => $_GPC ['keyword3code'],
            'keyword4code' => $_GPC ['keyword4code'],
            'keyword5code' => $_GPC ['keyword5code'],
            'keyword6code' => $_GPC ['keyword6code'],
            'keyword7code' => $_GPC ['keyword7code'],
            'keyword8code' => $_GPC ['keyword8code'],
            'keyword9code' => $_GPC ['keyword9code'],
            'keyword10code' => $_GPC ['keyword10code'],
            'remark' => $_GPC ['remark'],
            'remarkcolor' => $_GPC ['remarkcolor']
        );
        if ($item ) {
            $objTmplmsg->update($data,$id);
            message ( '消息模板修改成功！', $this->createWebUrl ( 'tmplmsg'), 'success' );
        } else {
            $objTmplmsg->insert($data);
            message ( '消息模板添加成功！', $this->createWebUrl ( 'tmplmsg'), 'success' );
        }
    }
    include $this->template ( 'tmplmsg' );
}
// 消息模板修改
// 消息模板删除
elseif($op=='delete')
{
    load ()->func ( 'tpl' );
    if (! empty ( $id )) {
        $item=$objTmplmsg->fetch($id);
        if (! empty ( $item )) {
            $objTmplmsg->delete($id);
            message ( '消息模板删除成功', referer (), 'success' );
        } else {
            message ( '消息模板不存在或已删除', referer (), 'error' );
        }
    } else {
        message ( '系统出错', referer (), 'error' );
    }
}