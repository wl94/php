<?php
global $_W,$_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include_once MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
checklogin();

$uniacid=$_W['uniacid'];
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$memberObj=M('member');
$id=intval($_GPC['id']);
if($operation=='display'){
    $psize=15;
    $pindex = max(1, intval($_GPC['page']));
    $list=$memberObj->fetchpageall($pindex," and memberId<>''" ,array());
    $total =$memberObj->fetchtotalnum();
    $pager = pagination($total, $pindex, $psize);
}


include $this->template('member');
