<?php
global $_W, $_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include_once MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
checklogin();

$uniacid=$_W['uniacid'];
load()->func('tpl');

$modulename = $this->modulename;
$storeid = intval($_GPC['storeid']);
$condition = '';
$objStoreslide=M('storeslide');
$photos = $objStoreslide->fetchall(" and storeid=:storeid {$condition} ",array(':storeid' => $storeid));

if (checksubmit('submit')) {
    if (!empty($_GPC['attachment-new'])) {
        foreach ($_GPC['attachment-new'] as $index => $row) {
            if (empty($row)) {
                continue;
            }
            $data = array(
                'uniacid' => $uniacid,
                'description' => $_GPC['description-new'][$index],
                'storeid' => $storeid,
                'attachment' => $_GPC['attachment-new'][$index],
                'url' => $_GPC['url-new'][$index],
                'isfirst' => intval($_GPC['isfirst-new'][$index]),
                'displayorder' => $_GPC['displayorder-new'][$index],
                'status' => $_GPC['status-new'][$index],
            );
            $objStoreslide->insert($data);
        }
    }
    if (!empty($_GPC['attachment'])) {
        foreach ($_GPC['attachment'] as $index => $row) {
            if (empty($row)) {
                continue;
            }
            $data = array(
                'uniacid' => $uniacid,
                'title' => $_GPC['title'][$index],
                'description' => $_GPC['description'][$index],
                'attachment' => $_GPC['attachment'][$index],
                'url' => $_GPC['url'][$index],
                'isfirst' => intval($_GPC['isfirst'][$index]),
                'displayorder' => $_GPC['displayorder'][$index],
                'status' => $_GPC['status'][$index],
            );
            $objStoreslide->update($data,$index);
        }
    }
    message('幻灯片更新成功！', $this->createWebUrl('slide', array('storeid' => $storeid)));
}
include $this->template('storeslide');
