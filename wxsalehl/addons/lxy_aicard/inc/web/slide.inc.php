<?php
global $_W, $_GPC;
include_once MODULE_ROOT.'/inc/common/global.fun.php';
include_once MODULE_ROOT.'/inc/core/model.php';
include_once MODULE_ROOT.'/const.php';
checklogin();

$uniacid=$_W['uniacid'];
load()->func('tpl');
$operation = ! empty($_GPC['op']) ? $_GPC['op'] : 'display';
$objSlide=M('slide');
if ($operation == 'display') {
    $pindex = max(1, intval($_GPC['page']));
    $list=$objSlide->fetchpageall($pindex);
    $psize=15;

    $total =$objSlide->fetchtotalnum();
    $pager = pagination($total, $pindex, $psize);

} elseif ($operation == 'post') {
    $id = intval($_GPC['id']);
    if (checksubmit('submit')) {
        $data = array(
            'uniacid' =>$uniacid,
            'advname' => $_GPC['advname'],
            'link' => $_GPC['link'],
            'enabled' => intval($_GPC['enabled']),
            'displayorder' => intval($_GPC['displayorder']),
            'thumb' => $_GPC['thumb']
        );
        if (! empty($id)) {
            $objSlide->update($data,$id);
        } else {
            $id=$objSlide->insert($data);
        }
        message('更新幻灯片成功！', $this->createWebUrl('slide', array(
            'op' => 'display'
        )), 'success');
    }

    $adv=$objSlide->fetch($id);
} elseif ($operation == 'delete') {
    $id = intval($_GPC['id']);
    $adv=$objSlide->fetch($id);
    if (!$adv) {
        message('抱歉，幻灯片不存在或是已经被删除！', $this->createWebUrl('slide', array(
            'op' => 'display'
        )), 'error');
    }
    $objSlide->delete($id);
    message('幻灯片删除成功！', $this->createWebUrl('slide', array(
        'op' => 'display'
    )), 'success');
} else {
    message('请求方式不存在');
}
include $this->template('slide', TEMPLATE_INCLUDEPATH, true);
