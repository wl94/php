
<?php

global $_W;
defined('IN_IA') or exit('Access Denied');
define('LXY_MODULE_NAME', 'lxy_aicard');
define('ISDEBUG',true);

if (!defined('MODULE_ROOT')) {
    define('MODULE_ROOT', dirname(__FILE__));
}
if (!defined('MODULE_URL')) {
    define('MODULE_URL', $_W['siteroot'].'addons/'.LXY_MODULE_NAME.'/');
}
 $_img_url = '../addons/'.LXY_MODULE_NAME.'/template/images/';

 $_css_url = '../addons/'.LXY_MODULE_NAME.'/template/css/';

 $_script_url = '../addons/'.LXY_MODULE_NAME.'/template/js/';

 $_pic_url = '../addons/'.LXY_MODULE_NAME.'/template/picture/';

define(EARTH_RADIUS, 6371); //地球半径，平均半径为6371km
define('RES', '../addons/lxy_aicard/template');


$_table_member='lxy_aicard_member';
$_table_stores='lxy_aicard_stores';

