<?php
/**
 * 微路会员卡模块微站定义
 *
 * @author 大路货
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');

class Lxy_aicardModuleSite extends WeModuleSite {
    public $_img_url = '../addons/lxy_aicard/template/img/';
    public $_css_url = '../addons/lxy_aicard/template/css/';
    public $_script_url = '../addons/lxy_aicard/template/js/';
    public $_pic_url = '../addons/lxy_aicard/template/picture/';

    public function doMobileIndex() {
        //这个操作被定义用来呈现 功能封面
    }
    public  function get_fields(){
        $setting=$this->module['config'];
        $cfgString=trim($setting['customsfield']);
        if($cfgString){
            $fields=explode(',',$cfgString);
        }

        return $fields;
    }
    public function get_pos($colid,$rowid){
        $pos=chr($colid + 65 ) . $rowid;
        return $pos;
    }

    public function getLatLng(){
        $arr=$_COOKIE['LXY_AROUNDSHOP_CURRENTCITY'];
        return json_decode($arr,true);
    }

    public  function map_bd2tx($lat,  $lon){
        $x_pi=3.14159265358979324;
        $x = $lon - 0.0065;
        $y = $lat - 0.006;
        $z = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $x_pi);
        $theta = atan2($y, $x) - 0.000003 * cos($x * $x_pi);
        $tx_lon = $z * cos($theta);
        $tx_lat = $z * sin($theta);
        return array($tx_lat,$tx_lon);
    }

    public function getInterfaceUrl($type,$key,$par1,$par2){
        //$baseUrl=$this->module['config']['interfaceurl'];
        $baseUrl='http://localhost:8088/crm';
        $url='';
        switch ($type){
            case 'member':
                //根据手机号获取会员信息
                $url=$baseUrl.'/api/wechat/member?phoneNum='.$key.'&openId='.$par1.'&wxAccount='.$par2;
                break;
            case 'coupons':
                //获取会员优惠券（包含现金券和优惠券）
                $url=$baseUrl.'/api/wechat/member/coupons?memberId='.$key;
                break;
            case 'coupon':
                //获取会员优惠券（包含现金券和优惠券）
                $url=$baseUrl.'/api/wechat/member/coupon/'.$key;
                break;
            case 'rebate':
                //获取会员优惠券（包含现金券和优惠券）
                $url=$baseUrl.'/api/wechat/member/rebate/'.$key;
                break;
            case 'associateOrgs':
                //获取优惠券适应机构
                $url=$baseUrl.'/api/wechat/member/coupon/associateOrgs?couponPromoId='.$key;
                break;
            case 'associateProducts':
                $url=$baseUrl.'/api/wechat/member/coupon/associateProducts?couponPromoId='.$key;
                break;
            case 'associateMembers':
                $url=$baseUrl.'/api/wechat/member/coupon/associateMembers?couponPromoId='.$key;
                break;
            case 'storesnearby':
                $url=$baseUrl.'/api/wechat/storesNearBy?longitude='.$key.'&latitude='.$par1.'&radius='.$par2;
                break;
            case 'storesAll':
                $url=$baseUrl.'/api/wechat/storesAll';
                break;
            case 'register':
                $url=$baseUrl.'/api/wechat/register?userinfo='.$key.'&openId='.$par1.'&wxAccount='.$par2;
                break;
            case 'updateMember':
                $url=$baseUrl.'/api/wechat/updateMember?userinfo='.$key;
                break;
            case 'queryMember':
                $url=$baseUrl.'/api/wechat/queryMember?memberId='.$key;
                break;    
        }
        return $url;
    }


    public function fansSendall($arrcondition, $msgtype, $media_id,$uniacid) {
       $types = array('text' => 'text', 'image' => 'image', 'news' => 'mpnews', 'voice' => 'voice', 'video' => 'mpvideo', 'wxcard' => 'wxcard');
        if(empty($types[$msgtype])) {
            return error(-1, '消息类型不合法');
        }
        $is_to_all = false;

        if(!count($arrcondition )) {
            return error(-1,'请选择您发送的范围');
        }
        $condition='';
        $param=array(
            ':uniacid'=>$uniacid
        );
        if($arrcondition['province']){
            $condition.=' and province=:province ';
            $param[':province']=$arrcondition['province'];
        }
        if($arrcondition['city']){
            $condition.=' and city=:city ';
            $param[':city']=$arrcondition['city'];
        }
        if($arrcondition['district']){
            $condition.=' and district=:district ';
            $param[':district']=$arrcondition['district'];
        }
        if($arrcondition['sex']){
            $condition.=' and sex=:sex ';
            $param[':sex']=$arrcondition['sex']==2?0:$arrcondition['sex'];
        }

        $allopenid=pdo_fetchall('select openid from '.tablename('lxy_aicard_member')." where uniacid=:uniacid {$condition} ",$param);


        if(!$allopenid){
            return error(-1, "筛选范围内没有符合的会员用户！");
        }
        foreach ($allopenid as $open){
            $arr[]=$open['openid'];
        }
        $data = array(
            'touser' => $arr,
            $types[$msgtype] => array(
                'media_id' => $media_id
            ),
            'msgtype' => $types[$msgtype]
        );

        if($msgtype == 'wxcard') {
            unset($data['wxcard']['media_id']);
            $data['wxcard']['card_id'] = $media_id;
        }
        $acc = WeAccount::create();

        $token = $acc->getAccessToken();
        if(is_error($token)){
            return $token;
        }
        $url="https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={$token}";//根据openid群发

        $data = urldecode(json_encode($data));
        $response = ihttp_request($url, $data);
        if(is_error($response)) {
            return error(-1, "访问公众平台接口失败, 错误: {$response['message']}");
        }
        $result = @json_decode($response['content'], true);
        if(empty($result)) {
            return error(-1, "接口调用失败, 元数据: {$response['meta']}");
        } elseif(!empty($result['errcode'])) {
            return error(-1, "访问微信接口错误, 错误代码: {$result['errcode']}, 错误信息: {$result['errmsg']},错误详情：{$this->error_code($result['errcode'])}");
        }
        $result['fansnum']=count($allopenid);//返回用户数量
        return $result;
    }

    function getInterfaceData($type,$key,$par1='',$par2=''){
        $url=$this->getInterfaceUrl($type,$key,$par1,$par2);
        load()->func('communication');

        $rsp=ihttp_get($url);
        /*$rsp='';
        if($type=='register'){
            $rsp=ihttp_post($url,$key);
        }else{
            $rsp=ihttp_get($url);
        }*/
        $dat=$rsp['content'];
        $arrdat=json_decode($dat,true) ;
        return $arrdat;
    }
}