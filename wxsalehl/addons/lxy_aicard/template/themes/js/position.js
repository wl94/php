/**
 * Created by jasonwolf on 16-4-28.
 */
//全局访问点，获取当前位置
$(function () {

    function getLocationcontent() {
        var cur_pos = $.cookie('lxy_simshop_CURRENTCITY');
        var def_pos = $.cookie('lxy_simshop_DEFAULTCITY');

        if (cur_pos == null && def_pos == null) {
            getLocation(setLocationInput, setLocationInput);
        } else {
            setLocationInput();
        }
    }

//把当前位置设置在input框中
    var setLocationInput = function (parama) {

        var cur_pos = $.cookie('lxy_simshop_CURRENTCITY');

        if (cur_pos == null) {
            alert('请开启cookie!');
            return;
        }

        var write_pos = JSON.parse(cur_pos);


        $('#curlat').val(write_pos.lat);
        $('#curlng').val(write_pos.lng);
        $('#curmsg').val(write_pos.result);

        //获取列表
        get_shops();
    }
//清除地点
    var clearPostion = function () {
        alert('重新定位!');
        $.cookie('lxy_simshop_DEFAULTCITY', null, {path: '/'});
        $.cookie('lxy_simshop_CURRENTCITY', null, {path: '/'});
        getLocationconteten();

    }
    var getLocation = function (successFunc, errorFunc) { //successFunc获取定位成功回调函数，errorFunc获取定位失败回调
        //首先设置默认城市
        var defCity = {
            result: 0,
            lat: 29.8852590000,
            lng: 121.5790060000,
            date: curDateTime()//获取当前时间方法
        };
        var expiresDate = new Date();
        var setmin = $(".cookiemin").val();
        setmin = setmin ? setmin : 5;
        expiresDate.setTime(expiresDate.getTime() + (setmin * 60 * 1000));


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                    var lat = position.coords.latitude;
                    var lng = position.coords.longitude;

                    //当前定位城市
                    var curCity = {
                        result: 1,
                        lat: lat,
                        lng: lng,
                        date: curDateTime()//获取当前时间方法
                    };


                    //GPS坐标
                    var ggPoint = new BMap.Point(lng, lat);

                    //坐标转换完之后的回调函数
                    console.log('原:' + lat + '--' + lng);

                    translateCallback = function (data) {
                        if (data.status === 0) {

                            curCity.lat=data.points[0].lat;
                             curCity.lng=data.points[0].lng;
                             console.log('终:'+curCity.lat+'--'+curCity.lng);

                        }
                        $.cookie('lxy_simshop_CURRENTCITY', JSON.stringify(curCity), {expires: expiresDate, path: '/'});
                        if (successFunc != undefined)
                            successFunc(curCity);
                    }

                    setTimeout(function () {
                        var convertor = new BMap.Convertor();
                        console.log('step2');
                        var pointArr = [];
                        pointArr.push(ggPoint);
                        convertor.translate(pointArr, 1, 5, translateCallback)
                    }, 1000);


                },
                function (error) {
                    switch (error.code) {
                        case 1:
                            alert("位置服务被拒绝。");
                            break;
                        case 2:
                            alert("暂时获取不到位置信息。");
                            break;
                        case 3:
                            alert("获取位置信息超时。");
                            break;
                        default:
                            alert("未知错误。");
                            break;
                    }

                    //默认城市
                    $.cookie('lxy_simshop_CURRENTCITY', JSON.stringify(defCity), {expires: expiresDate, path: '/'});
                    if (errorFunc != undefined)
                        errorFunc(error);
                }, {timeout: 5000, enableHighAccuracy: true});
        } else {
            alert("你的浏览器不支持获取地理位置信息。");
            if (errorFunc != undefined)
                errorFunc("你的浏览器不支持获取地理位置信息。");
        }
    };


    function curDateTime() {
        var d = new Date();
        var year = d.getYear();
        var month = d.getMonth() + 1;
        var date = d.getDate();
        var day = d.getDay();
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();
        var ms = d.getMilliseconds();
        var curDateTime = year;
        if (month > 9)
            curDateTime = curDateTime + "-" + month;
        else
            curDateTime = curDateTime + "-0" + month;
        if (date > 9)
            curDateTime = curDateTime + "-" + date;
        else
            curDateTime = curDateTime + "-0" + date;
        if (hours > 9)
            curDateTime = curDateTime + " " + hours;
        else
            curDateTime = curDateTime + " 0" + hours;
        if (minutes > 9)
            curDateTime = curDateTime + ":" + minutes;
        else
            curDateTime = curDateTime + ":0" + minutes;
        if (seconds > 9)
            curDateTime = curDateTime + ":" + seconds;
        else
            curDateTime = curDateTime + ":0" + seconds;
        return curDateTime;
    }

    getLocationcontent();

});