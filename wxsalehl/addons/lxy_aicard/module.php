<?php
/**
 * 微路会员卡模块定义
 *
 * @author 大路货
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');

class Lxy_aicardModule extends WeModule {

    public $modulename = 'lxy_simshop';
    public $_debug = '1';

    public function settingsDisplay($settings) {
        global $_GPC, $_W;
        $tmps = pdo_fetchall("SELECT * FROM " . tablename('lxy_aicard_tmplmsg') . " WHERE uniacid = :uniacid ORDER BY `id` asc", array(
            ':uniacid' => $_W['uniacid']
        ));


        if(checksubmit()) {
            $cfg = $_GPC['cfgvalue'];
            $cfg['location_p']=$_GPC['location_p'];
            $cfg['location_c']=$_GPC['location_c'];
            $cfg['location_a']=$_GPC['location_a'];
            $cfg['lng']=$_GPC['baidumap']['lng'];
            $cfg['lat']=$_GPC['baidumap']['lat'];
            if($this->saveSettings($cfg)) {
                message('保存成功', 'refresh');
            }
        }
        include $this->template('setting');
    }

}